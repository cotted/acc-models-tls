/******************************************************************
 **
 ** ELENA injection from AD => settings taken from Davide Gamba's files
 **
 **  M. Fraser and F. Velotti: calculating initial conditions for injection from LNI
 ******************************************************************/

option, RBARC=FALSE;

/******************************************************************
 * Call lattice files
 ******************************************************************/

! Path needs updating when moving into repository

option, -warn;
call, file = "elena_repo/elements/ELENA_elements.def";
call, file = "elena_repo/sequence/ELENA_ring.seq";

/*****************************************************************************
 * Set quadrupolar strength as given during design
 *****************************************************************************/
 !!! Qx=2.3,Qy=1.3, gap=76mm, E1=E2=Pi*17/180, FINT=0.424, Lbm=0.927m
 KQ1:= 2.27646e+00;
 KQ2:=-1.20793e+00;
 KQ3:= 7.19841e-01;
 
 
/*******************************************************************************
 * beam
 *******************************************************************************/

 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0053; ! Assuming AD extraction energy of 5.3 MeV

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;
 
 /******************************************************************
 * Twiss, determine injection location and install marker downstream of kicker
 ******************************************************************/
 
 use, sequence=elena;
 twiss;

 sInjection = table(twiss,LNR.MKKFH.0120,s);
 value, sInjection;
 
 pointInjection: marker;
 
 SEQEDIT, SEQUENCE=ELENA;
       	FLATTEN;
        INSTALL, ELEMENT=pointInjection, AT= sInjection, FROM=ELENA$START;
        FLATTEN;
 ENDEDIT;
 
  SEQEDIT, SEQUENCE=ELENA;
       	FLATTEN;
        CYCLE, START=pointInjection;
        FLATTEN;
 ENDEDIT;
 
EXTRACT, sequence=ELENA, FROM=pointInjection, TO=LNR.BTVPA.0118, newname=ELENA_INJECT;
 
calculate_injection(ring_twiss_file) : macro = {  

    select, flag = twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
    use, sequence = ELENA_INJECT;
    twiss, beta0 = tl_final_cond, table = twiss_ring;
    
    write, table=twiss_ring, file="ring_twiss_file";

};

calculate_match(matched_ring_twiss_file) : macro = {  

    select, flag = twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
    use, sequence = ELENA;
    savebeta, label=ring_match_cond, place = pointInjection;
    twiss, table = matched_twiss_ring;
    
    write, table=matched_twiss_ring, file="matched_ring_twiss_file";
    
    
    /***********************************************
    * Save ring parameters to file for TL matching usage
    ***********************************************/

    assign, echo="elena_matched.inp";
    
    betx0 = ring_match_cond->betx;
    bety0 =  ring_match_cond->bety;

    alfx0 = ring_match_cond->alfx;
    alfy0 = ring_match_cond->alfy;

    dx0 = ring_match_cond->dx;
    dy0 = ring_match_cond->dy;

    dpx0 = ring_match_cond->dpx;
    dpy0 = ring_match_cond->dpy;

    x0 = ring_match_cond->x;
    y0 = ring_match_cond->y;

    px0 = ring_match_cond->px;
    py0 = ring_match_cond->py;

    mux0 = ring_match_cond->mux;
    muy0 = ring_match_cond->muy;

    print, text="/*********************************************************************";
    print, text="Initial conditions for injection from MADX model of ELENA";
    print, text="*********************************************************************/";

    print, text = '';
    value,betx0;
    value,bety0;

    value,alfx0;
    value,alfy0;

    value,dx0 ;
    value,dy0 ;

    value,dpx0;
    value,dpy0;

    value,x0 ;
    value,px0 ;

    assign, echo=terminal;

};