!==============================================================================================
! MADX file for LNS-LNE optics
!
! M.A. Fraser, D. Gamba, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "ELENA/LNS-LNE optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/
system, "ln -fns ../../../../acc-models-elena elena_repo";
system, "ln -fns ./../../../elena_extraction/lne lne_repo";
system, "ln -fns ./../../delnslni delnslni_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

/*****************************************************************************
 Calculate initial condition for matching in the ring
*****************************************************************************/

call, file = "lne_repo/load_elena_injection00.madx";

set, format="22.6e";
exec, calculate_match(twiss_elena_matched.tfs);

!exec, set_ini_conditions();


/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNS
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "delnslni_repo/lns/lns.ele";
 call, file = "delnslni_repo/lns/lns_lne_k.str";
 call, file = "delnslni_repo/lns/lns.seq";
 !call, file = "delnslni_repo/lns/lns.dbx"; !Presently no aperture database: to be updated
 option, echo;
 
 /*****************************************************************************
 * LNE00
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;
 
 lns.zdsia.0030.E: marker;
 
 SEQEDIT, SEQUENCE=LNE00;
       	FLATTEN;
        INSTALL, ELEMENT=lns.zdsia.0030.E, AT= -0.3, FROM=lns.zdsia.0030;
        FLATTEN;
 ENDEDIT;
  
 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lns.zdsia.0030.E, NEWNAME=lne00tolnssurvey;
 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lns.zdsia.0030.E, NEWNAME=lne00tolns;
 
 ! Redfine ion switch for a deflection to the right:
 LNS.ZDSIA.0030_43.00: EXSIA_43;
 
 ! Redfine extraction kicker for a deflection to the right:
 lnr.zdfa.0610: exbendr220;
 
 SEQEDIT, SEQUENCE=lne00tolns; REFLECT; FLATTEN; ENDEDIT;
 
 /*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 lnslne00: sequence, refer=ENTRY, l = 2.8264 + 4.4628;
   lns                         , at =      0;
   lne00tolns                  , at = 2.8264;
 endsequence;

 SEQEDIT, SEQUENCE=lnslne00; FLATTEN; ENDEDIT;

 /*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
 X0=0;
 PX0=0;
 Y0=0;
 PY0=0;
 PT0=0;

 /*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./elena_source.inp";
 exec, set_ini_conditions();
 
/*******************************************************************************
 * Run twiss for LNS-LNE00 and stitch result
 *******************************************************************************/

use, sequence= lnslne00;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=tl_final_cond, place = LNE.START.0000;
twiss, beta0=initbeta0, file = "twiss_lns_lne.tfs", table = twiss_lns_lne;

len_twiss = table(twiss_lns_lne, tablelength);
value, len_twiss;
    
i = 2;
option, -info;
create,table=trajectory, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
while(i < len_twiss){
SETVARS, TABLE=twiss_lns_lne, ROW=i;
x0 = x;
px0 = px;

fill, table=trajectory;

i = i + 1;

};

set, format="22.6e";
exec, calculate_injection(twiss_elena_stitched.tfs);

! Make one single tfs file for both ring and transfer line 

len_twiss_ring = table(twiss_ring, tablelength);
value, len_twiss_ring;

i = 2;
option, -info;
while(i < len_twiss_ring){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss_ring, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_lns_lne_elena_nom_complete.tfs";


/*************************************
* Cleaning up
*************************************/

system, "rm lne_repo";
system, "rm elena_repo";
system, "rm delnslni_repo";

stop;
