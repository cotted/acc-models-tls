!==============================================================================================
! MADX file for LNE06 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "ELENA/LNE06 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/
system, "ln -fns ../../../../acc-models-elena elena_repo";
system, "ln -fns ./../../lne lne_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;
 

/*****************************************************************************
 Calculate live initial condition for KR or any other changes in the ring
  - For now only macro to evaluate changes in ZDFA.0310
*****************************************************************************/

call, file = "lne_repo/load_elena_extraction00.madx";
length.ELENA_EXTRACT = table(summ, length);

! It needs as input as ZDFA.0310 delta kick (absolute value in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! if "sign" (second argument) = 1, positive kick, negative otherwise

set, format="22.6e";
exec, calculate_extraction(0e-3, 1, twiss_elena_stitched.tfs);

exec, set_ini_conditions();

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/

exec, write_ini_conditions(0,0,tl_initial_cond,elena_stitched.inp);

/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNE00
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00_k.str";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;

 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lne.lne00.lne01, NEWNAME=lne00to01;
option, -echo, -twiss_print;
use, sequence=lne00to01;
twiss, betx=1, bety=1;
lne00to01.length = table(summ, length);
 
/*******************************************************************************
 * LNE01 line
 *******************************************************************************/
 call, file = "lne_repo/lne01/lne01.ele";
 call, file = "lne_repo/lne01/lne01_k.str";
 call, file = "lne_repo/lne01/lne01.seq";
 !call, file = "lne_repo/lne01/lne01.dbx"; !Presently no aperture database: to be updated
 
 EXTRACT, SEQUENCE=lne01, FROM=lne.start.0100, TO=lne.lne01.lne06, NEWNAME=lne01to06;
option, -echo, -twiss_print;
use, sequence=lne01to06;
twiss, betx=1, bety=1;
lne01to06.length = table(summ, length);


/*******************************************************************************
 * LNE06 line
 *******************************************************************************/
 call, file = "lne_repo/lne06/lne06.ele";
 call, file = "lne_repo/lne06/lne06_k.str";
 call, file = "lne_repo/lne06/lne06.seq";
 !call, file = "lne_repo/lne06/lne06.dbx"; !Presently no aperture database: to be updated
option, -echo, -twiss_print;
use, sequence=lne06;
twiss, betx=1, bety=1;
lne06.length = table(summ, length);
option, echo, twiss_print;
 
/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 lne00lne01lne06: sequence, refer=ENTRY, l = lne00to01.length + lne01to06.length + lne06.length;
   lne00to01              , at =        0;
   lne01to06              , at = lne00to01.length;
   lne06                  , at = lne00to01.length + lne01to06.length;
  endsequence;
 SEQEDIT, SEQUENCE=lne00lne01lne06; FLATTEN; ENDEDIT;

 lnrlne06: sequence, refer=ENTRY, l = length.ELENA_EXTRACT + lne00to01.length + lne01to06.length + lne06.length;
   ELENA_EXTRACT          , AT =  0.0000000000 ;
   lne00to01              , at = length.ELENA_EXTRACT;
   lne01to06              , at = length.ELENA_EXTRACT + lne00to01.length;
   lne06                  , at = length.ELENA_EXTRACT + lne00to01.length + lne01to06.length;
  endsequence;
 SEQEDIT, SEQUENCE=lnrlne06; FLATTEN; ENDEDIT;


EXTRACT, SEQUENCE=lne06, FROM=LNE.LNE06.LNE05, TO=LNE.END.0663, NEWNAME=lne06.transfer;
SEQEDIT, SEQUENCE=lne06.transfer; FLATTEN; ENDEDIT;

/*******************************************************************************
 * Run twiss for LNE06 and stitch result
 *******************************************************************************/
savebeta, label=lne06_transfer_start, place=LNE.LNE06.LNE05;

use, sequence= lne00lne01lne06;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lne00_lne01_lne06.tfs";

! Make one single tfs file for both ring and FTN transfer line using kickers
len_twiss_tl = table(twiss, tablelength);

i = 2;
option, -info;
while(i < len_twiss_tl){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_elena_lne00_lne01_lne06_nom_complete.tfs";

/***********************************************************
* JMAD: prepare single sequences
************************************************************/
set, format="22.14e";
option, -warn;
save, sequence=lne06.transfer, beam, file='jmad/lne06.jmad', newname=lne06;

save, sequence=lnrlne06, beam, file='jmad/lnrlne06.jmad', newname=lnrlne06;
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp lnr_start.inp jmad";
exec, write_ini_conditions(0,0,lne06_transfer_start,lne06_transfer.inp);
system, "mv lne06_transfer.inp jmad/lne06_start.inp";

/*************************************
* Cleaning up
*************************************/

system, "rm lne_repo";
system, "rm elena_repo";

stop;
