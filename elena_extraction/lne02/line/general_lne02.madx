!==============================================================================================
! MADX file for LNE02 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LNE02 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../lne lne_repo";

/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));

 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;

 
/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";
 
/*****************************************************************************
 * LNE00
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00_k.str";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;

 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lne.lne00.lne01, NEWNAME=lne00to01;

/*******************************************************************************
 * LNE01 line
 *******************************************************************************/
 call, file = "lne_repo/lne01/lne01.ele";
 call, file = "lne_repo/lne01/lne01_k.str";
 call, file = "lne_repo/lne01/lne01.seq";
 !call, file = "lne_repo/lne01/lne01.dbx"; !Presently no aperture database: to be updated
 
 EXTRACT, SEQUENCE=lne01, FROM=lne.start.0100, TO=lne.lne01.lne02, NEWNAME=lne01to02;

/*******************************************************************************
 * LNE02 line
 *******************************************************************************/
 call, file = "lne_repo/lne02/lne02.ele";
 call, file = "lne_repo/lne02/lne02_k.str";
 call, file = "lne_repo/lne02/lne02.seq";
 !call, file = "lne_repo/lne02/lne02.dbx"; !Presently no aperture database: to be updated


/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 lne00lne01lne02: sequence, refer=ENTRY, l = 10.6361309+6.2625788+7.653745125;
   lne00to01              , at =        0;
   lne01to02              , at = 10.6361309;
   lne02                  , at = 10.6361309+6.2625788;
  endsequence;

 SEQEDIT, SEQUENCE=lne00lne01lne02; FLATTEN; ENDEDIT;

/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./../stitched/elena_stitched.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * MATCH: uncomment to run matching
 *******************************************************************************

lne02match: macro={
select,flag=twiss,column=name,s,betx,bety,alfx,alfx,dx,dpx,dy,dpy,mux,muy,l,k0l,k1l;
OPTION, sympl = false;
twiss, beta0=initbeta0, file = "twiss_lne00_lne01_lne02_match.tfs";
};

use, sequence= lne00lne01lne02;
match,vlength=false,use_macro;

vary,name=KLNE.ZQMD.0208,step=1e-6,lower=-54,upper=54;
vary,name=KLNE.ZQMD.0209,step=1e-6,lower=-54,upper=54;
vary,name=KLNE.ZQMF.0214,step=1e-6,lower=-54,upper=54;
vary,name=KLNE.ZQMD.0215,step=1e-6,lower=-54,upper=54;

use_macro,name=lne02match;
emith = 6 ;
emitv = 4 ;
dppi = 2.5;
beamsize = 2.5;
constraint,weight=1,range=#e,expr= sqrt((table(twiss,LNE02$END,betx)*emith/6)+(beta*table(twiss,LNE02$END,dx)*dppi/4)^2) < beamsize;
constraint,weight=1,range=#e,expr= sqrt((table(twiss,LNE02$END,bety)*emitv/6)+(beta*table(twiss,LNE02$END,dy)*dppi/4)^2) < beamsize;

constraint,range=lne02$start/lne02$end, bety < 100;
constraint,range=lne02$start/lne02$end, betx < 50;

simplex,calls=5000,tolerance=1e-6;



endmatch;

*******************************************************************************/


/*******************************************************************************
 * twiss
 *******************************************************************************/
use, sequence= lne00lne01lne02;
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lne00_lne01_lne02_nom.tfs";

/*************************************
* Survey
*************************************/

 set_su_ini_conditions(xx) : macro = {

 x00 = table(survey,xx,X);
 y00 = table(survey,xx,Y);
 z00 = table(survey,xx,Z);
 theta00 = table(survey,xx,THETA);
 phi00 = table(survey,xx,PHI);
 psi00 = table(survey,xx,PSI);

 };
 
call, file = "./make_survey_lne02.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm lne_repo";

stop;
