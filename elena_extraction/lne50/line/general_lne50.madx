!==============================================================================================
! MADX file for LNE50 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LNE50 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../lne lne_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;
 
/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNE50
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne50/lne50.ele";
 call, file = "lne_repo/lne50/lne50_k.str";
 call, file = "lne_repo/lne50/lne50.seq";
 !call, file = "lne_repo/lne50/lne50.dbx"; !Presently no aperture database: to be updated
 option, echo;


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
call, file = "./../stitched/lne50_start.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * MATCH
 *******************************************************************************
 use, sequence= lne50;  
 MATCH , sequence=lne50, beta0 = INITBETA0;
        constraint, sequence=lne50,range=#e,betx=6.313973;
        constraint, sequence=lne50,range=#e,alfx=-1.791306;
        constraint, sequence=lne50,range=#e,bety=2.845569;
        constraint, ,sequence=lne50,range=#e,alfy=-0.946093;
        vary , NAME=klne.zqmd.5010;
        vary , NAME=klne.zqmf.5011;
        vary , NAME=klne.zqmf.5050;
        vary , NAME=klne.zqmd.5051;
        LMDIF, calls = 10000 , tolerance = 1.d-6;
ENDMATCH;
 
value, klne.zqmd.5010;
value, klne.zqmf.5011;
value, klne.zqmf.5050;
value, klne.zqmd.5051;
  

 *******************************************************************************/

/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence= lne50;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lne50_nom.tfs";

/*************************************
* Survey
*************************************/
call, file = "./make_survey_lne50.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm lne_repo";

stop;
