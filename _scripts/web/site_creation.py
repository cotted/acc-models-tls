import jinja2
import datetime
import os
import sys
import glob

import pandas as pnd
import numpy as np
import matplotlib.pyplot as plt

from bokeh.plotting import figure, output_file, output_notebook, show, save, ColumnDataSource
from bokeh.models import Legend, LinearAxis, Range1d, CustomJS, Slider, Span, Panel, Tabs
from bokeh.models.glyphs import Rect
from bokeh.layouts import row, column, gridplot
import bokeh.palettes


# Pass path to tls repository
local_path = sys.argv[1]

# Read branch from file written frim CI
with open('branch.txt', 'r') as f:
    branch = f.readlines()[0][:-1]


def readtfs(filename, usecols=None, index_col=0, check_lossbug=True):
    header = {}
    nskip = 1
    closeit = False

    try:
        datafile = open(filename, 'r')
        closeit = True
    except TypeError:
        datafile = filename

    for line in datafile:
        nskip += 1
        if line.startswith('@'):
            entry = line.strip().split()
            header[entry[1]] = eval(' '.join(entry[3:]))
        elif line.startswith('*'):
            colnames = line.strip().split()[1:]
            break

    if closeit:
        datafile.close()

    table = pnd.read_csv(filename, delim_whitespace = True,
                        skipinitialspace = True, skiprows = nskip,
                        names = colnames, usecols = usecols,
                        index_col = index_col)

    if check_lossbug:
        try:
            table['ELEMENT'] = table['ELEMENT'].apply(lambda x: str(x).split()[0])
        except KeyError:
            pass
        try:
            for location in table['ELEMENT'].unique():
                if not location.replace(".","").replace("_","").replace('$','').isalnum():
                    print("WARNING: some loss locations in "+filename+
                          " don't reduce to alphanumeric values. For example "+location)
                    break
                if location=="nan":
                    print("WARNING: some loss locations in "+filename+" are 'nan'.")
                    break
        except KeyError:
            pass

    return header, table


def scenarios(directory):
    # Removed scenarios and actually using the root directory => inside made the divison per date!!
    basedir = directory
    scns = next(os.walk(basedir))[1]

    scns = [ele for ele in scns if ele.endswith('tion')]

    scenarios = pnd.DataFrame(columns=['label', 'short_desc', 'desc', 'config_list', 'configs', 'dir'],
                              index=scns)

    for scn in scns:
        aux = scn.split('_')

        if len(aux) == 1:
            scenarios['label'].loc[scn] = scn
        elif len(aux) == 2:
            if aux[0] == '':
                scenarios['label'].loc[scn] = aux[1]
            else:
                scenarios['label'].loc[scn] = aux[0] + ' ' + aux[1]

        scenarios['dir'].loc[scn] = basedir.split('repository')[-1][1:] + scn
        configs = configurations(scn, directory)

        scenarios['config_list'].loc[scn] = configs.index.tolist()
        scenarios['configs'].loc[scn] = [configs.to_dict(orient="index")]

    return scenarios.replace(np.nan, '', regex=True)


def configurations(scn, directory):
    basedir = directory + scn + '/'
    print('Importing data from scenario: ' + scn)

    # For transfer lines are all the different TL optics of the specific TL, e.g. 'tof' for 'ps_extraction'
    configs = [config.split('/')[0] for config in sorted(os.listdir(basedir)) if 'supplementary' not in config]

    print('configs: ', configs)

    types = ['line', 'stitched']

    index = pnd.MultiIndex.from_product([configs, types], names=['configs', 'type'])

    configurations = pnd.DataFrame(columns=['label', 'madx', 'str', 'tfs', 'ele', 'dbx', 'inp',
                                            'twiss', 'momentum', 'BTV_names',
                                            's', 'betx', 'bety', 'dx', 'dy', 'mux', 'muy',
                                            'plot_pdf', 'plot_html', 'plot_height'],
                                   index=index)
    for config in configs:
        for line_type in types:
            # Making the label as configuration_type (ex. TT2TT10_LHC_line)
            configurations['label'].loc[config, line_type] = config + '_' + line_type

            try:
                # For TLs needed more file type
                label = ['madx', 'str', 'tfs', 'ele', 'dbx', 'inp']
                for lab in label:
                    files = glob.glob(basedir + config + '/' + line_type + '/*' + lab)
                    if lab == 'madx':
                        files = [ele for ele in files if 'general' in ele]
                    elif lab == 'tfs':
                        file_tfs = [ele for ele in files if 'tfs' in ele]
                        files = [ele for ele in files if 'nom' in ele]
                    try:
                        configurations[lab][config, line_type] = files[0].split('/')[-1]
                    except:
                        configurations[lab][config, line_type] = ''
                print('MADX conf', configurations['madx'][config, line_type])
                if not configurations['madx'][config, line_type] == '':

                    plot_html_file = basedir + config + '/' + line_type + '/' + \
                                     configurations['madx'][config, line_type].split('.')[0] + '.html'

                    plot_file = basedir + config + '/' + line_type + '/' + \
                                configurations['madx'][config, line_type].split('.')[0] + '.pdf'

                    twiss_file_name_nom = configurations['tfs'][config, line_type]
                    twiss_file_name_nom = basedir + config + '/' + line_type + '/' + twiss_file_name_nom

                    # Make spacial case for TT20 SFTPRO
                    if 'tt20t2_sftpro' in twiss_file_name_nom and line_type == "line":
                        info, twiss = readtfs(basedir + config + '/' + line_type + '/' + 'twiss_tt20tt21_sftpro.tfs',
                            index_col=None)
                        _, twiss1 = readtfs(basedir + config + '/' + line_type + '/' + 'twiss_tt21tt22_sftpro.tfs',
                            index_col=None)
                        _, twiss2 = readtfs(basedir + config + '/' + line_type + '/' + 'twiss_tt22tt23_sftpro.tfs',
                            index_col=None)

                        twiss1['S'] += twiss['S'].iloc[-1]
                        twiss2['S'] += twiss1['S'].iloc[-1]

                        twiss = pnd.concat([twiss, twiss1, twiss2])

                    else:
                        info, twiss = readtfs(twiss_file_name_nom, index_col=None)
                        isPtc = False
                        if 'BETA11' in twiss.columns:
                            twiss = twiss.rename(columns={'BETA11': 'BETX',
                                                          'BETA22': 'BETY'})
                        if 'MU1' in twiss.columns:
                            twiss = twiss.rename(columns={'MU1': 'MUX',
                                                          'MU2': 'MUY'})
                        if 'DISP1' in twiss.columns:
                            twiss = twiss.rename(columns={'DISP1': 'DX',
                                                          'DISP3': 'DY'})
                            isPtc = True
                            
                            
                    twiss.index = twiss['NAME']
                    configurations['twiss'][config, line_type] = twiss

                    if 'PC' in info.keys():
                        # Twiss file is a file with proper summary table
                        configurations['momentum'][config, line_type] = np.round(info['PC'], 2)
                    else:

                        # Look for file in line directory...if any
                        path_line = basedir + config + '/line/'
                        try:
                            files_line = os.listdir(path_line)
                        except FileNotFoundError:
                            files_line = None
                        if files_line:
                            for file in files_line:
                                if 'twiss' in file:
                                    info, _ = readtfs(path_line + file)
                                    try:
                                        configurations['momentum'][config, line_type] = np.round(info['PC'], 2)
                                        print('++++++++++++++++Info: gamma from line!')
                                    except:
                                        print('++++++++++++++++Warning: gamma not found!')
                                        configurations['momentum'][config, line_type] = 100.0
                                        info['GAMMA'] = np.sqrt(100**2 + 0.938**2) / 0.938
                        else:
                            found = False
                            for file in file_tfs:
                                print(f'File in stitched: {file}')
                                info_temp, _ = readtfs(file)
                                if 'PC' in info_temp.keys():
                                    found = True
                                    configurations['momentum'][config, line_type] = np.round(info_temp['PC'], 2)
                                    print('++++++++++++++++Info: gamma from other twiss!')
                                    info = info_temp.copy()
                            if not found:
                                print('++++++++++++++++Warning: gamma not found!')
                                configurations['momentum'][config, line_type] = 100.0
                                info['GAMMA'] = np.sqrt(100**2 + 0.938**2) / 0.938


                    gamma = info['GAMMA']
                    
                    # PTC TWISS has DX = dX/d(dp/p) instead of pt as MADX
                    beta = 1.0 if isPtc else np.sqrt(1 - (1/gamma**2))

                    configurations['BTV_names'][config, line_type] = [name for name in twiss['NAME'] if ('BTV'
                                                                      in name or 'BSG' in name or 'MTV' in name)]
                    optics = get_optics_at_location(twiss, configurations['BTV_names'][config, line_type], beta)

                    for k in optics.keys():
                        configurations[k][config, line_type] = np.round(optics[k], 3)

                    configurations['plot_pdf'].loc[config, line_type] = plot_file.split('/')[-1]
                    configurations['plot_html'].loc[config, line_type] = plot_html_file.split('/')[-1]

                    # create plots only if twiss file is more recent than existing plot file
                    try:
                        make_plot = os.path.getctime(twiss_file_name_nom) > os.path.getctime(plot_html_file)
                        make_plot = True
                    except:
                        make_plot = True
                    if make_plot:
                        print('making pdf...')
                        create_plots(twiss, plot_file, beta)
                        if line_type == 'stitched':
                            create_bokeh_plots(twiss, plot_html_file, stitched=True, beta=beta)
                        else:
                            create_bokeh_plots(twiss, plot_html_file, beta=beta)

                    if line_type == 'stitched':
                        configurations['plot_height'].loc[config] = 800
                    else:
                        configurations['plot_height'].loc[config] = 600

            except IndexError as e:
                print(e)
                pass

    return configurations


def supplementary(directory):
    suppl = [suppl.split('/')[-1] for suppl in sorted(glob.glob(directory + 'supplementary/*'))]
    print(suppl)

    supplementary = pnd.DataFrame(columns=['label'],
                                  index=suppl)

    for sup in suppl:
        aux = sup.split('_')

        if len(aux) == 1:
            supplementary['label'].loc[sup] = sup
        elif len(aux) == 2:
            if aux[0] == '':
                pass
            else:
                supplementary['label'].loc[sup] = aux[0] + ' ' + aux[1]

    return supplementary.replace(np.nan, '', regex=True)


def renderfile(dirnames, name, template, data):
    basedir = ''

    for dirname in dirnames:
        if type(dirname) == tuple:
            for ele in dirname:
                basedir = os.path.join(basedir, ele)
        else:
            basedir = os.path.join(basedir, dirname)

    fullname = os.path.join(basedir, name)

    with open(fullname, 'w') as indexfile:
        indexfile.write(template.render(**data))

    print("Successfully created " + fullname)

def get_optics_at_location(twiss, BI_names, beta=1):
    optics = {'s': [], 'betx': [], 'bety': [], 'dx': [], 'dy': [], 'mux': [], 'muy': []}
    for name in BI_names:
        for key in optics.keys():
            if 'd' in key:
                optics[key].append(twiss[key.upper()].loc[name] * beta)
            else:
                try:
                    optics[key].append(twiss[key.upper()].loc[name])
                except KeyError:
                    optics[key].append(0.0)

    return optics

def create_plots(twiss, filename, beta=1):
    plt.figure(figsize=(6, 3))

    plt.plot(twiss.S, twiss.BETX, label=r'$\beta_x$', )
    plt.plot(twiss.S, twiss.BETY, label=r'$\beta_y$')
    p0 = plt.plot(twiss.S, twiss.DX * 0, ls='--', label=r'$D_x$')
    p1 = plt.plot(twiss.S, twiss.DY * 0, ls='--', label=r'$D_y$')
    plt.ylim([np.min((twiss.BETX.min(), twiss.BETY.min())) * 0.8, np.max((twiss.BETX.max(), twiss.BETY.max())) * 1.1])
    plt.xlabel('s / m')
    plt.ylabel(r'$\beta_{x,y}$ / m')
    plt.legend(frameon=False, ncol=4, loc='upper center', bbox_to_anchor=(0.5, 1.12))

    plt.twinx()

    plt.plot(twiss.S, twiss.DX * beta, c=p0[0].get_color(), ls='--', alpha=0.5, label=r'$D_x$')
    plt.plot(twiss.S, twiss.DY * beta, c=p1[0].get_color(), ls='--', alpha=0.5, label=r'$D_y$')
    plt.ylim([np.min((twiss.DX.min(), twiss.DY.min())) * 1.1, np.max((twiss.DY.max(), twiss.DX.max())) * 1.1])

    plt.ylabel(r'$D_{x, y}$ / m')

    print('saving...')

    plt.savefig(filename, dpi=150, bbox_inches='tight')


def create_Columndatasource(parameters, values):
    # parameters and data have to be a list

    data = pnd.DataFrame(columns = parameters)
    for i, p in enumerate(parameters):
        data[p] = values[i]

    return ColumnDataSource(data)


def plot_lattice_elements(figure, twiss, filename):
    print('Plotting lattice for HTML plot')
    pos = twiss.S - twiss.L/2
    lengths = twiss.L
    # modify lengths in order to plot zero-length elements
    lengths[np.where(lengths == 0)[0]] += 0.001

    # BENDS
    idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if ('BEND' in elem) or ('MATRIX' in elem)])
    print('BENDS done')

    idx_0 = idx[twiss.K1L[idx] == 0]
    # distinguish F and D half-units of the PS
    idx_1 = idx[twiss.K1L[idx] > 0]
    idx_2 = idx[twiss.K1L[idx] < 0]

    cols = ['#2ca25f', bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
    for i, indx in enumerate([idx_0, idx_1, idx_2]):
        source = create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME)[indx]])
        figure.rect(x='pos', y=0, width='width', height=2, fill_color=cols[i], line_color='black', source=source)

    # QUADRUPOLES
    idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'QUADRUPOLE' in elem])
    name = np.array(twiss.NAME)[idx]
    
    idx_1 = idx[twiss.K1L[idx] > 0]
    idx_2 = idx[twiss.K1L[idx] < 0]
    print('QUADS done')

    cols = [bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
    offset = [0.6, -0.6]
    for i, indx in enumerate([idx_1, idx_2]):
        source = create_Columndatasource(['pos', 'width', 'name'], [pos[indx], lengths[indx], np.array(twiss.NAME)[indx]])
        figure.rect(x = 'pos', y = 0 + offset[i], width = 'width', height = 1.2, fill_color=cols[i], line_color = 'black', source = source)

    # # KICKERS
    # idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if 'KICKER' in elem])
    # source = create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME)[idx]])
    # figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color='#1b9e77', line_color = 'black', source = source)

    # MONITORS and INSTRUMENTS
    idx = np.array([idx for idx, elem in enumerate(twiss.KEYWORD) if ('MONITOR' in elem) or ('INSTRUMENT' in elem)])
    source = create_Columndatasource(['pos', 'width', 'name'], [pos[idx], lengths[idx], np.array(twiss.NAME)[idx]])
    figure.rect(x = 'pos', y = 0, width = 'width', height = 2, fill_color='gray', line_color = 'black', source = source)
    print('Instrument done')

    # horizontal line at zero
    source = create_Columndatasource(['pos', 'name'], [[0, twiss.S[-1]], ['START', 'END']])
    figure.line('pos', 0., line_width=.5, line_color = 'black', source = source)
    print('Elements for HTML plot created')

def create_bokeh_plots(twiss, filename, stitched=False, beta=1):
    # definition of parameters to be shown when hovering the mouse over the data points
    tooltips = [("parameter", "$name"), ("element", "@name"), ("value [m]", "$y")]
    tooltips_elements = [("element", "@name")]

    # output to static HTML file
    output_file(filename, mode="inline")

    # define the datasource
    data = pnd.DataFrame(columns=['s', u"\u03B2x", u"\u03B2y", "Dx", "Dy"])
    data['s'] = twiss.S
    data[u"\u03B2x"] = twiss.BETX
    data[u"\u03B2y"] = twiss.BETY
    data['Dx'] = twiss.DX * beta
    data['Dy'] = twiss.DY * beta
    data['x'] = twiss.X * 1e3
    data['y'] = twiss.Y * 1e3
    data['name'] = twiss.NAME

    # calculate plot limits based on data range
    # beta-functions:
    bmin = (np.floor(np.min([data[u"\u03B2x"], data[u"\u03B2y"]]) / 5)) * 5
    bmax = (np.floor(np.max([data[u"\u03B2x"], data[u"\u03B2y"]]) / 5) + 1) * 5
    b_p2p = bmax - bmin
    y = Range1d(start=bmin - b_p2p / 2, end=bmax)

    # dispersion-function:
    dxmin = (np.floor(np.min([data['Dx'].min(), data['Dy'].min()]) * 1.2))
    dxmax = (np.floor(np.max([data['Dx'].max(), data['Dy'].max()]) * 1.2))
    dx_p2p = dxmax - dxmin
    dx = Range1d(start=dxmin, end=dxmax + dx_p2p * 2)

    # create a new plot with a title and axis labels
    f = figure(title="", x_axis_label='s [m]', y_axis_label=u'\u03B2-functions [m]', width=800, height=500,
               x_range=Range1d(0, twiss.S.max(), bounds="auto"), y_range=y, tools="box_zoom, pan, reset, hover",
               active_drag='box_zoom', tooltips=tooltips)
    f.xaxis.axis_label_text_font_size = "15pt"
    f.yaxis.axis_label_text_font_size = "15pt"
    f.axis.major_label_text_font = 'times'
    f.axis.axis_label_text_font = 'times'
    f.axis.axis_label_text_font_style = 'normal'
    f.outline_line_color = 'black'
    #     f.sizing_mode = 'scale_width'

    source = ColumnDataSource(data)
    cols = ['darkblue', 'salmon', 'green', 'black']
    for i, col in enumerate(data.columns[1:3]):
        print(col)
        f.line('s', col, source=source, name=col, line_width=1.5, line_color=cols[i])

    # Setting the second y axis range name and range
    f.extra_y_ranges = {"disp": dx}
    f.extra_x_ranges = {"sections": Range1d(start=0.0001, end=twiss.S.max(), bounds="auto")}

    # Adding the second x axis to the plot.
    # f.add_layout(
    #     LinearAxis(x_range_name="sections", axis_label='Start of straight section', axis_label_text_font='times',
    #                axis_label_text_font_style='normal', major_label_text_font='times'), 'above')

    # Adding the second y axis to the plot.
    f.add_layout(LinearAxis(y_range_name="disp", axis_label='D [m]', axis_label_text_font='times',
                            axis_label_text_font_style='normal', major_label_text_font='times'), 'right')
    dx = f.line('s', 'Dx', source=source, name='Dx', line_width=1.5, line_color='black', y_range_name="disp")
    dy = f.line('s', 'Dy', source=source, name='Dy', line_width=1.5, line_color='orange', y_range_name="disp")
    f.yaxis.axis_label_text_font_size = "15pt"
    legend = Legend(items=[(u"\u03B2x", [f.renderers[0]]), (u"\u03B2y", [f.renderers[1]]), ("Dx", [f.renderers[2]]),
                           ("Dy", [f.renderers[3]])], location=(10, 165))
    f.add_layout(legend, 'right')
    legend.label_text_font = 'times'

    f0 = figure(title="", width=800, height=40, x_range=f.x_range, y_range=(-1.25, 1.25),
                tools="box_zoom, pan, reset, hover", active_drag='box_zoom', tooltips=tooltips_elements)

    f0.axis.visible = False
    f0.grid.visible = False
    f0.outline_line_color = 'white'
    f0.sizing_mode = 'scale_width'

    f0.toolbar.logo = None
    f0.toolbar_location = None

    plot_lattice_elements(f0, twiss, filename)
    print('Saving HTML plot')
    if stitched:

        tooltips = [("parameter", "$name"), ("element", "@name"), ("value [mm]", "$y")]

        f1 = figure(title="", x_axis_label='s [m]', y_axis_label='x, y [mm]', width=800, height=150,
                    x_range=f.x_range,
                    y_range=(np.floor(min(data['x']) / 10) * 10, (np.floor(max(data['x']) / 10) + 1) * 10),
                    tools="box_zoom, pan, reset, hover", active_drag='box_zoom', tooltips=tooltips)

        f1.axis.major_label_text_font = 'times'
        f1.axis.axis_label_text_font = 'times'
        f1.axis.axis_label_text_font_style = 'normal'
        f1.outline_line_color = 'black'
        #         f1.sizing_mode = 'scale_width'

        # f1.yaxis.major_tick_line_color = None
        f1.line('s', 'x', source=source, name='x', line_width=1.5, line_color='black')
        f1.line('s', 'y', source=source, name='y', line_width=1.5, line_color='red')

        f1.toolbar.logo = None
        f1.toolbar_location = None
        # f.xaxis.axis_label = None
        # f.xaxis.major_label_text_color = None
        # f.xaxis.major_tick_line_color = None
        # f.xaxis.minor_tick_line_color = None

        # save the results
        save(column(f0, f, f1))
    else:
        # save the results
        save(column(f0, f))


def get_desc(row_dir, label, scns):
    count = 0
    try:
        desc_file = row_dir + scns.loc[label]['dir'] + '/description.txt'
        with open(desc_file, 'r') as f:
            desc = {}
            for line in f.readlines():
                if hasattr(line, '__len__'):
                    if len(line) > 1:
                        if line.split()[0] == '#':
                            desc_type = line.split()[1]
                            desc[desc_type] = []
                            count += 1
                        else:
                            if count > 0:
                                desc[desc_type].append(line.replace('\n', ''))
        for key in desc.keys():
            scns.loc[label][key] = ' '.join(desc[key])
    except FileNotFoundError:
        print('++++ ', label, ': no description found +++++')
        print(desc_file, ' missing...')
        print('Please consider to add description.txt inside you directory')
    return scns

# %%

year = datetime.datetime.now().year

repo_directory = local_path
row_dir = repo_directory.replace('tls/', '')

scns = scenarios(repo_directory)


# %%

suppl = supplementary(repo_directory)

suppl.loc['documentation']['label'] = 'documentation and useful links'

# %% ---------------------------------------


# Read description files and add to scns
for label in scns.index:
    scns = get_desc(row_dir, label, scns)

# %% ---------------------------------------

# add TLs names and paths to the dataframe of scenarios
scns['tl_labels'] = ''
scns['tl_paths'] = ''
for idx, row in scns.iterrows():
    tl_labels = []
    tl_paths = []
    for ele in row['config_list']:
        print(idx, ele)
        print(row['configs'][0][ele]['madx'])
        if row['configs'][0][ele]['madx'] != '':
            tl_labels.append(' '.join(list(ele)).upper())
            path = '/'.join(list(ele))
            tl_paths.append(path)

    scns['tl_labels'].loc[idx] = [tl_labels]
    scns['tl_paths'].loc[idx] = [tl_paths]
# %% ---------------------------------------


templateLoader = jinja2.FileSystemLoader(searchpath=local_path + "./_scripts/web/templates/")
templateEnv = jinja2.Environment(loader=templateLoader)

tmain = templateEnv.get_template("main.template")
tscen = templateEnv.get_template("scenario.template")
tconf = templateEnv.get_template("configuration.template")
tyml = templateEnv.get_template('mkdocs.yml.template')

rdata = {
    'date': datetime.datetime.now().strftime("%d/%m/%Y"), 
    'year': str(year), 
    'scenarios': scns,
    'supplementary': suppl, 
    'branch': branch
}

print('\nCreating websites...\n')
renderfile([repo_directory], 'index.md', tmain, rdata)

# %% ---------------------------------------
renderfile([repo_directory], 'nav.yml', tyml, rdata)

# %% ---------------------------------------

# various parameters to be included in the tables for each configuration
beam_data = ['p [GeV/c]']

# parameters of interest at the BI equipments
BTV_info = ['s [m]', '&beta;<sub>x</sub> [m]', '&beta;<sub>y</sub> [m]', 'D<sub>x</sub> [m]',
            'D<sub>y</sub> [m]', '&mu;<sub>x</sub>', '&mu;<sub>y</sub>']

for idx, scn in scns.iterrows():
    rdata['scn'] = scn
    rdata['BTV_info'] = BTV_info

    basedir = repo_directory
    renderfile([basedir, scn.name], 'index.md', tscen, rdata)

    for config in scn['config_list']:
        if scn['configs'][0][config]['madx'] != '':
            conf = scn.configs[0][config]
            rdata['conf'] = conf
            rdata['BI_names'] = scn['configs'][0][config]['BTV_names']
            #         rdata['resdir']=os.path.join(basedir,scn.name,conf.name)
            #         print conf.label,conf.settings['ip1b1']

            renderfile([basedir, scn.name, config], 'index.md', tconf, rdata)

print("Done!")
