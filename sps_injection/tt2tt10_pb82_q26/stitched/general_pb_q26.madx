/***********************************************************
 * MADX optics model for fully stripped Pb beam transfer from TT10 to SPS
 * Based on files from AFS repository 
 *
 * F.Velotti
 **********************************************************/
 title, "TT2/TT10 Pb82+ Q26 optics.";

 option, echo;
 option, RBARC=FALSE;  ! the length of a rectangular magnet
                       ! is the distance between the polefaces
                       ! and not the arc length

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm tt10sps_lhc_q26_savedseq.seq";
system, "rm ./jmad/*";

/***************************************
* Load needed repos
***************************************/
system,"[ ! -e sps_repo ] && [ -d ./../../sps_repo ] && ln -nfs ./../../sps_repo sps_repo";
system,"[ ! -e sps_repo ] && [ -d /afs/cern.ch/eng/acc-models/sps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/sps/2021 sps_repo";
system,"[ ! -e sps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-sps -b 2021 sps_repo";

system, "ln -fns ./../../../ps_extraction/tt10 tt10_repo";
system, "ln -fns ./../../../ps_extraction/tt2tt10_pb82_q26/line tt2tt10_pb_repo";

/*******************************************************************************
 * Beam
 *******************************************************************************/
A      = 208;    ! sum of protons and neutrons
Beam, particle=lead,pc=6.75*A,charge=54, mass=A*0.9315, BUNCHED; !(no emittance defined yet)
BRHO = 57.4;

/*******************************************************************************
 * TT10
 *******************************************************************************/

 call, file = "./tt10_repo/tt10.ele";
 call, file = "./tt2tt10_pb_repo/tt10_fe_pb_strip.str";
 call, file = "./tt10_repo/tt10.seq";
 call, file = "./tt10_repo/tt10.dbx";

/*******************************************************************************
 * set initial twiss parameters
 *******************************************************************************/
! Legacy file...to be used until re-matching done
call, file = "./../../../ps_extraction/tt2tt10_pb82_q26/line/tt10_pb_strip.inp";

/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
start_tt10: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;



/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence=tt10;

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=start_tt10, file="./twiss_tt10_pb_q26.tfs";


len_tt10 = table(twiss, ENDVKNV11839, s);

/*******************************************************************************
 * Load SPS model
 *******************************************************************************/

call, file = "sps_repo/sps.seq";
call, file = "sps_repo/strengths/lhc_q26.str";

 ENDTT10 : MARKER;
 ENDVKNV : MARKER;

 SEQEDIT, SEQUENCE=SPS;
    remove, element = bipmv.51694;
    install, element = ENDVKNV, at = -0.925, from = BTV.11860;
 ENDEDIT;

/*******************************************************************************
 * Make SPS twiss file
 *******************************************************************************/


use, sequence = sps;
 ! MSI settings to optimise aperture
 kmsi    = -0.0432612 /4;

 ! MKP settings
 mkp_volt = 52;
 rebalancing = 0.0; ! Only 3 generators


 Bdl_mkpa_50 = 0.0883 ;
 Bdl_mkpc_50 = 0.0353 ;
 Bdl_mkp_50 = 0.133 ;

 mkpa_kv2rad = (1 / 50) * Bdl_mkpa_50 / BRHO;
 mkpc_kv2rad = (1 / 50) * Bdl_mkpc_50 / BRHO;
 mkpl_kv2rad = rebalancing * (1 / 50) * Bdl_mkpl_50 / BRHO;

 ! Injection bump
 bump_inj = 4.94 ;
 k1_b               =  1.34169e-05 ;
 k2_b               = -1.42917e-06 ;
 k3_b               =  1.34230e-05 ;

/***********
! For matching of bump
  match, sequence = SPS;
    vary, name = kMDH11607, step = 1e-6;
    vary, name = kMDH11831, step = 1e-6;
    vary, name = kMDH12007, step = 1e-6;


    constraint, range = mdh.11607, x=0.0;
    constraint, range = MDH.12007, x=0.0;
    constraint, range = qf.11810, x= 0.00137 ;

   lmdif, calls =1000, tolerance =1e-8;
 endmatch;

k1_b =  kMDH11607;
k2_b =  kMDH11831;
k3_b =  kMDH12007;
*********/

 kMDH11607 :=   k1_b * bump_inj;
 kMDH11831 :=   k2_b * bump_inj;
 kMDH12007 :=   k3_b * bump_inj;

use, sequence = sps;
select, flag = twiss, clear;
savebeta, label=end_inj, place = end.10010;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, file="./twiss_sps_pb_q26.tfs";

len_sps = table(twiss, end.10010, s);

/*******************************************************************************
 * Reverse SPS for extraction and get (x, px) at injection
 *******************************************************************************/
seqedit, sequence = sps; 
	extract, sequence=sps, from=endvknv, to=end.10010, newname=spsinj;
endedit;

use, sequence = spsinj;

seqedit, sequence=spsinj;
    flatten;
    reflect;
    flatten;
endedit;

kmkp11955  := mkp_volt * mkpl_kv2rad;
kmkpa11931 := mkp_volt * mkpa_kv2rad;
kmkpa11936 := mkp_volt * mkpa_kv2rad;
kmkpc11952 := mkp_volt * mkpc_kv2rad;

end_inj->alfx = -1 * end_inj->alfx;
end_inj->alfy = -1 * end_inj->alfy;
end_inj->px = -1 * end_inj->px;
end_inj->py = -1 * end_inj->py;
end_inj->dpx = -1 * end_inj->dpx;
end_inj->dpy = -1 * end_inj->dpy;

use, sequence = spsinj;
select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=end_inj, file="./twiss_sps_pb_q26_inj_rev.tfs";


x_inj  = table(twiss, ENDVKNV, x);
px_inj = -1 * table(twiss, ENDVKNV, px);

value, x_inj, px_inj;

change_ref: MATRIX, L=0,  kick1 = x_inj, kick2 = px_inj, rm26= px_inj/(beam->beta), rm51 = -px_inj/(beam->beta);

/*******************************************************************************
 * build up the complete model => TT10 + SPS
 *******************************************************************************/

use, sequence = sps;
! Changing reference at injection
 SEQEDIT, SEQUENCE=sps;
    install, element = change_ref, at = 1e-10, from = ENDVKNV;
    flatten;
 ENDEDIT;

use, sequence = sps;

SEQEDIT, SEQUENCE=SPS;
    flatten ; cycle, start=  ENDVKNV;
ENDEDIT;


/*******************************************************************************
 * Make nominal twiss of TT10 + SPS sequence and dump to file the sequence
 *******************************************************************************/

tt10sps: sequence, refer=entry, l = len_tt10 + len_sps;
    tt10                  , at = 0.0;
    sps, at = 0.0, from = ENDVKNV11839;
endsequence;


use, sequence = tt10sps;

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0 = start_tt10;
write, table=twiss, file="./twiss_tt10sps_pb_q26_nom.tfs";

show, start_tt10;

/***********************************************
* Save sequence and initial conditions for JMAD
***********************************************/

set, format="10.8f";
option, -warn;
save, sequence = tt10sps, file="./jmad/tt10sps_pb_q26_savedseq.seq", beam;
option, warn;

assign, echo="./jmad/tt10sps_pb_q26.inp";

betx0 = start_tt10->betx;
bety0 = start_tt10->bety;
alfx0 = start_tt10->alfx;
alfy0 = start_tt10->alfy;

dx0 = start_tt10->dx;
dy0 = start_tt10->dy;

dpx0 = start_tt10->dpx;
dpy0 = start_tt10->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction to TT10 for Pb82+ Q26 beam';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;
/************************************
* Cleaning up
************************************/
system, "rm tt10_repo";
system, "rm -rf sps_repo || rm sps_repo";
system, "rm tt2tt10_pb_repo";

stop;



