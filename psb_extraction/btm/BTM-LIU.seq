option, -echo;
option, info;
option, warn;
!---------------------------------------------------------------------------
! - The sequence starts exactly at the entry point of the former 2.2 m long
!   version of BT.BHZ10 and ends at the center of the dump core. The starting point
!   has been kept in the final version with the 2.000457 m long BT.BHZ10 magnet,
!   which means that the startiong point is roughly 10 cm upstream the entry of BT.BHZ10
!   and slightly outside its bounding box
!
! - BTM line is split into two parts:
!   1st part: BT.BHZ10 to BTY handover point (BTY.HOP) to the ISOLDE line
!   2nd part: BTY handover point to PSB beam dump
!
! - The position of the elements corresponds to the magnetic center
!   where applicable otherwise to the mechanical center of each element. All
!   positions are derived from the positions in the drawing PSBIHENS0031-vAC, except
!   the quadrupoles and the bending magnet BTM.BHZ10 which are derived from GEODE data.
!   Corrections are made to those values to start the line at the BTM starting point,
!   to take into account the differences between the beam path and the straight line
!   trajectory in BT.BHZ10 and in BTM.BHZ10 and to take into account differences in the beam
!   path lengths for different long versions of BT.BHZ10 (btbhz10.ds) and BTM.BHZ10 (btmbhz10.ds).
!
! - (IE) April 25, 2019
!   add an offset in BTM line to accomodate for the drift of 0.00021699399999874913 between the end of BT3 and 
!   start of BTM in order to correctly generate the survey files.
!  
!--------------------------------------------------------------------------

btmoffset = 0.00021699399999874913 ; ! offset to end of BT3
displa=0.5*(2.2-1.533832857 ) + btmoffset;

! (IE) offsets to match the pre-LS2 survey positions

BTBHZ10_offset_for_survey = -0.0010881789999999947+0.00014693300000000187;
BTMBHZ10_offset_for_survey = -0.00209; 
BTM_offset_for_survey = -0.00208 ;


!ISOpoint = 11.39010;!old
ISOpoint = 10.42678;! new BTM_2 starting point / ISOLDE handover point: upstream flange of BTY.BVT101, CH 16/11/2018

length_BTM_1 = ISOpoint+displa + 2*btbhz10.ds + 2*btmbhz10.ds + BTM_offset_for_survey;
!length_BTM_2 = 14.16450 + (11.39010-ISOPoint);
length_BTM_2 = 25.124820852 - length_BTM_1;

value, length_BTM_1;
value, length_BTM_2;


BTM_1: sequence, refer=centre, l = length_BTM_1;

        BT.BHZ10  , at = 0.7669165+displa + btbhz10.ds + BTBHZ10_offset_for_survey;  // Path length/2
        !BTM.VVS10 , at = 2.59700+displa + 2*btbhz10.ds + BTM_offset_for_survey;
        BTM.BTV10 , at = 2.94050+displa + 2*btbhz10.ds + 0.04201 + BTM_offset_for_survey;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.QNO05 , at = 3.52201+displa + 2*btbhz10.ds + BTM_offset_for_survey;
        ! BTM.BHZ10 , at = 5.40040+displa + 2*btbhz10.ds + btmbhz10.ds + BTMBHZ10_offset_for_survey;
        BTM.BHZ10 , at = 5.40040+displa + 2*btbhz10.ds + btmbhz10.ds + BTM_offset_for_survey;
        BTM.QNO10 , at = 7.20328+displa + 2*btbhz10.ds + 2*btmbhz10.ds + BTM_offset_for_survey;
        BTM.DVT10 , at = 7.94160+displa + 2*btbhz10.ds + 2*btmbhz10.ds-0.04 + 0.03718 + BTM_offset_for_survey;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.DHZ10 , at = 8.40560+displa + 2*btbhz10.ds + 2*btmbhz10.ds - 0.00282 + BTM_offset_for_survey;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.QNO20 , at = 9.08628+displa + 2*btbhz10.ds + 2*btmbhz10.ds + BTM_offset_for_survey;
        BTM.BPM00 , at = 9.63760+displa + 2*btbhz10.ds + 2*btmbhz10.ds+0.15 - 0.08692 + BTM_offset_for_survey;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.BTV15 , at = 10.01910+displa + 2*btbhz10.ds + 2*btmbhz10.ds + 0.01358 + 0.035 + BTM_offset_for_survey;! correction to be consistent with drawing PSBIHENS0031 (2017) and information received from S. Burger on 20.11.2018, CH 10/01/2019
        !BTM.VPI11 , at = 10.21910+displa + 2*btbhz10.ds + 2*btmbhz10.ds - 0.00332 + BTM_offset_for_survey;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        ! BTY hand over point. Calculation of position: S(BTM.QNO10, MADX) + (S(BTY.BVT101, drawing) - S(BTM.QNO10, drawing)) - L(BTY.BVT101)/2
	! 7.20328+(45.984-41.8)-1.921/2 = 10.42678
	!BTY.HOP,     at = 10.42678+displa + 2*btbhz10.ds + 2*btmbhz10.ds + BTM_offset_for_survey;

        !BTM.VC    , at = 11.39010+displa + 2*btbhz10.ds + 2*btmbhz10.ds;  // old hand over point to ISOLDE
endsequence;


BTM_2: sequence, refer=centre, l = length_BTM_2;
    BTY.START    , at = 11.38728 - ISOPoint - 0.874;
	BTY.BVT101   , at = 11.38728 - ISOPoint;! added CH 16/11/2018
        !BTM.VPI11a   , at = 12.81110 - ISOPoint - 0.00282;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.BSGH01   , at = 13.41678 - ISOPoint;! adapt position to position in current optics file, CH 16/11/2018
        BTM.BSGV01   , at = 13.41678 - ISOPoint;! adapt position to position in current optics file, CH 16/11/2018
        BTM.BSGH02   , at = 15.91678 - ISOPoint;! adapt position to position in current optics file, CH 16/11/2018
        BTM.BSGV02   , at = 15.91678 - ISOPoint;! adapt position to position in current optics file, CH 16/11/2018
        BTM.BPM10    , at = 16.78260 - ISOPoint - 0.07432;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.BTV20    , at = 17.82910 - ISOPoint + 0.04418;! correction to be consistent with drawing PSBIHENS0031 (2017) and information received from S. Burger on 20.11.2018, CH 16/11/2018
        BTM.BSGH03   , at = 18.41678 - ISOPoint;! adapt position to position in current optics file, CH 16/11/2018
        BTM.BSGV03   , at = 18.41678 - ISOPoint;! adapt position to position in current optics file, CH 16/11/2018
        ! BTM.MBL10, at = 18.7;
        BTM.BCT10    , at = 18.76710 - ISOPoint - 0.00282;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
        BTM.TDU10.ENTRYFACE, at = 25.55460 - ISOPoint - 0.08532 - 0.6764 - 0.75;
        BTM.TDU10    , at = 25.55460 - ISOPoint - 0.08532 - 0.6764;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018

endsequence;

BTM: sequence, refer=ENTRY , l = 25.124820852;
        BTM.START : MARKER, at = 0.0;
        BTM_1, at = 0.0 ;
        BTM_2, at = length_BTM_1;
endsequence;

seqedit, sequence = BTM;
flatten;
endedit;

option, warn;
option, -echo;
option, -info;
return;
