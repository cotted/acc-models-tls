!==============================================================================================
! MADX file for PSB-BT3-BTP LHC optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "PSB/BT3/BTP optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btp btp_repo";

! When running the stitched model with PS these symbolic links will be made instead

if (stitchps == 1){
system, "ln -fns ./../../../psb_extraction/bt bt_repo";
system, "ln -fns ./../../../psb_extraction/btp btp_repo";
};



/*******************************************************************************
 * Beam command
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*******************************************************************************
 * Macros
 *******************************************************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

write_ini_conditions(xtlgeode, pxtlgeode, beamname,filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of PSB extraction to BT lines";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};


/*****************************************************************************
 Calculate initial conditions for BT transfer line
*****************************************************************************/

call, file = "bt_repo/load_psb3_extraction_lhc.madx";

! In this example the KFA14's kick can be adjusted (absolute error  in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! If "sign" (second argument) = 1, positive kick, negative otherwise

set, format="22.6e";
exec, calculate_extraction(1e-3, 1, twiss_psb_stitched);    

/***********************************************************
* Define TL reference frame - to be updated with SU (GEODE) measured values
************************************************************/

! Until we have better information we assume the beam is aligned to BT in the nominal case
xtl = tl_initial_cond_nominal->x;
pxtl = tl_initial_cond_nominal->px;

/*****************************************************************************
 * BT3 and BTP
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-BTP-LIU.str";
 call, file = "bt_repo/LHC-LIU.str";
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT3_LIU.seq";
 call, file = "bt_repo/BT.dbx"; 
 call, file = "btp_repo/BTP.ele";
 call, file = "btp_repo/BTP.seq"; 
 call, file = "btp_repo/BTP.dbx"; 
 option, echo;
 

 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/

lpsbext = 139.110102;

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtp = 35.79972785;

lps = 628.3185;

bt3btp: SEQUENCE, refer=ENTRY, L  = lbt3 + lbtp;
	BT3, 	AT = 0 ;
	BTP, 	AT = lbt3;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt3btp;
FLATTEN;
ENDEDIT;

/***********************************************************
* Save initial parameters in PSB ring for JMAD
************************************************************/
exec, write_ini_conditions(0,0,psbstart,PSB3_START_LHC.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond,BT3_START_KICK_RESPONSE_LHC.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT3-BTP and stitch result for kick response
 *******************************************************************************/

use, sequence= bt3btp;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_kick_response, place = #e;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt3btp_lhc_kick_response_complete.tfs";

/***********************************************************
* Save parameters at handover point for injection to PS for kick response
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_kick_response,PS3_START_KICK_RESPONSE_LHC.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond_nominal,BT3_START_LHC.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT3-BTP and stitch result for nominal case
 *******************************************************************************/

use, sequence= bt3btp;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_nominal, place = #e;
savebeta,label=btp3_start_lhc, place = btp.start;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt3btp_lhc_nom_complete.tfs";

/***********************************************************
* Save parameters at BTP start for nominal case
************************************************************/

exec, write_ini_conditions(0,0,btp3_start_lhc,BTP3_START_LHC.inp);

/***********************************************************
* Save parameters at handover point for injection to PS for nominal case
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_nominal,PS3_START_LHC.inp);

/***********************************************************
* JMAD: prepare single sequences
************************************************************/

if (stitchps == 1){};
else {

/***********************************************************
* PI.SMH42.BTP renamed to PI.SMH42 in BTP sequence (otherwise
* YASP doesn't recognise the septum name)
************************************************************/

PI.SMH42: rbend,l:= PI.SMH42.btp->l,angle:= angleSMH42;

SEQEDIT, SEQUENCE=btp;
REPLACE, ELEMENT=PI.SMH42.btp, BY=PI.SMH42;
ENDEDIT;

SEQEDIT, SEQUENCE=bt3btp;
REPLACE, ELEMENT=PI.SMH42.btp, BY=PI.SMH42;
ENDEDIT;

EXTRACT, SEQUENCE=psb3, FROM=PSB3.START, TO=BR3.BT_START, NEWNAME=psb3_ext;

psbbt3btp: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt3 + lbtp;
    psb3_ext        , AT =  0.0000000000 ;
	bt3          	, AT =  lpsbext ;
    btp          	, AT =  lpsbext + lbt3;
ENDSEQUENCE;

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

SEQEDIT, SEQUENCE = psbbt3btp;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR3.BT_START;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE3KFA14L1 := kBE3KFA14L1REF;

set, format="22.14e";
use, sequence= psbbt3btp;  
option, -warn;
save, sequence=psbbt3btp, beam, noexpr, file='jmad/psbbt3btp_lhc.jmad';
option, warn;

set, format="22.14e";
use, sequence= bt3btp; 
option, -warn;
save, sequence=bt3btp, beam, noexpr, file='jmad/bt3btp_lhc.jmad';
option, warn;

set, format="22.14e";
use, sequence= btp;  
option, -warn;
save, sequence=btp, beam, noexpr, file='jmad/btp_lhc.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb3_start_lhc.inp jmad";
system, "cp bt3_start_lhc.inp jmad";
system, "cp btp3_start_lhc.inp jmad";

};

/*************************************
* Cleaning up
*************************************/

if (stitchps == 1){return;};
else {
system, "rm bt_repo";
system, "rm btp_repo";
system, "rm -rf psb_repo || rm psb_repo";
stop;};
