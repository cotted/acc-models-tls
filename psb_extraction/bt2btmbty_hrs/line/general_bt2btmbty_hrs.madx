!==============================================================================================
! MADX file for BT2-BTY-HRS  optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "BT2-BTY-HRS optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btm btm_repo";
system, "ln -fns ./../../bty bty_repo";


/*******************************************************************************
 * Beam
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*****************************************************************************
 * BT2 and BTM
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-LIU_dump.str";
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT2_LIU.seq";
 call, file = "bt_repo/BT.dbx";
 call, file = "btm_repo/BTM-LIU_dump.str";
 call, file = "btm_repo/BTM.ele";
 call, file = "btm_repo/BTM-LIU.seq"; 
 call, file = "btm_repo/BTM.dbx"; 
 call, file = "bty_repo/BTY_HRS.str";
 call, file = "bty_repo/BTY.ele";
 call, file = "bty_repo/BTY.seq"; 
 !call, file = "btm_repo/BTY.dbx";  ! Needs to be created
 option, echo;

/*******************************************************************************
! Set initial twiss parameters
 *******************************************************************************/
 
call, file = "./../stitched/bt2_start_lhc.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/
lpsbext = 139.110102;

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtmbty = 10.84522;

lbty2ts2 = 110.9231473;

! BTY start marker at upstream flange of BTY.BVT10
EXTRACT, SEQUENCE=btm, FROM=BTM.START, TO=BTY.START, NEWNAME=btm2bty;

bt2btmbtyhrs: SEQUENCE, refer=ENTRY, l  =  lbt2 + lbtmbty + lbty2ts2;
	           bt2       , at =     0;
	           btm2bty    , at =  lbt2;
               bty2ts2    , at =  lbt2 + lbtmbty;              
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt2btmbtyhrs;
FLATTEN;
ENDEDIT;


/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence = bt2btmbtyhrs; 
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_bt2btmbty_hrs_nom.tfs";


/*************************************
* Survey
*************************************/
set_su_ini_conditions(xx) : macro = {

 x00 = table(survey,xx,X);
 y00 = table(survey,xx,Y);
 z00 = table(survey,xx,Z);
 theta00 = table(survey,xx,THETA);
 phi00 = table(survey,xx,PHI);
 psi00 = table(survey,xx,PSI);

 };
 
call, file = "./make_survey_bt2btmbty_hrs.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm bt_repo";
system, "rm btm_repo";
system, "rm bty_repo";

stop;
