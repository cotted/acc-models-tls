!==============================================================================================
! JMAD input files
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

title, "JMAD script to generate sequence: PSB/BT1/BTM/BTY optics";

/***************************************
* Load saved sequences for JMAD
***************************************/
option, -warn;
call, file = "psbbt1btmbty_hrs.jmad";
option, warn;

/***************************************
* Load initial conditions 
***************************************/
call, file = "psb1_start_lhc.inp";

/**************************************
 * Macros
***************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

exec, set_ini_conditions();

/***********************************************************
* Generate JMAD twiss
************************************************************/

use, sequence= psbbt1btmbtyhrs;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=INITBETA0, file = 'psbbt1btmbty_hrs.twiss';

/***************************************
* Load saved sequences for JMAD
***************************************/
option, -warn;
call, file = "bt1btmbty_hrs.jmad";
option, warn;

/***************************************
* Load initial conditions 
***************************************/
call, file = "bt1_start_lhc.inp";

exec, set_ini_conditions();

/***********************************************************
* Generate JMAD twiss
************************************************************/

use, sequence= bt1btmbtyhrs;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=INITBETA0, file = 'bt1btmbty_hrs.twiss';

/***************************************
* Load saved sequences for JMAD
***************************************/
option, -warn;
call, file = "bty_hrs.jmad";
option, warn;

/***************************************
* Load initial conditions 
***************************************/
call, file = "bty1_start_lhc.inp";

exec, set_ini_conditions();

/***********************************************************
* Generate JMAD twiss
************************************************************/

use, sequence= bty2ts2;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=INITBETA0, file = 'bty_hrs.twiss';