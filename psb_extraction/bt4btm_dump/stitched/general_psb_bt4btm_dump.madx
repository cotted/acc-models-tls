!==============================================================================================
! MADX file for PSB-BT4-BTM dump optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "PSB/BT4/BTM dump optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btm btm_repo";
system, "ln -fns ./../../bty bty_repo";

/*******************************************************************************
 * Beam command
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*******************************************************************************
 * Macros
 *******************************************************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

write_ini_conditions(xtlgeode, pxtlgeode, beamname,filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of PSB extraction to BT lines";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};


/*****************************************************************************
 Calculate initial conditions for BT transfer line
*****************************************************************************/

call, file = "bt_repo/load_psb4_extraction_lhc.madx";

! In this example the KFA14's kick can be adjusted (absolute error  in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! If "sign" (second argument) = 1, positive kick, negative otherwise

set, format="22.6e";
exec, calculate_extraction(1e-3, 1, twiss_psb_stitched);    

/***********************************************************
* Define TL reference frame - to be updated with SU (GEODE) measured values
************************************************************/

! Until we have better information we assume the beam is aligned to BT in the nominal case
xtl = tl_initial_cond_nominal->x;
pxtl = tl_initial_cond_nominal->px;

/*****************************************************************************
 * BT4, BTM and BTY
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-LIU_dump.str";
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT4_LIU.seq";
 call, file = "bt_repo/BT.dbx";
 call, file = "bty_repo/BTY.str";
 call, file = "bty_repo/BTY.ele";
 call, file = "bty_repo/BTY.seq";
 call, file = "btm_repo/BTM-LIU_dump.str";
 call, file = "btm_repo/BTM.ele";
 call, file = "btm_repo/BTM-LIU.seq"; 
 call, file = "btm_repo/BTM.dbx"; 
 option, echo;
 

 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/
lpsbext = 139.110102;

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtm = 25.124820852;

bt4btm: SEQUENCE, refer=ENTRY, l  =  lbt4 + lbtm;
	           bt4       , at =     0;
	           btm       , at =  lbt4;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt4btm;
FLATTEN;
ENDEDIT;

/***********************************************************
* Save initial parameters in PSB ring for JMAD
************************************************************/
exec, write_ini_conditions(0,0,psbstart,PSB4_START_LHC.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond,BT4_START_KICK_RESPONSE_LHC.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT4-BTM and stitch result for kick response
 *******************************************************************************/

use, sequence= bt4btm;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_kick_response, place = #e;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt4btm_dump_kick_response_complete.tfs";

/***********************************************************
* Save parameters at BTM dump for kick response
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_kick_response,BTM_DUMP_KICK_RESPONSE.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond_nominal,BT4_START_LHC.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT4-BTM and stitch result for nominal case
 *******************************************************************************/

use, sequence= bt4btm;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_nominal, place = #e;
savebeta,label=btm4_start_lhc, place = btm.start;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt4btm_dump_nom_complete.tfs";

/***********************************************************
* Save parameters at BTM start for nominal case
************************************************************/

exec, write_ini_conditions(0,0,btm4_start_lhc,BTM4_START_LHC.inp);

/***********************************************************
* Save parameters at BTM dump for nominal case
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_nominal,BTM_DUMP.inp);

/***********************************************************
* JMAD: prepare single sequences
************************************************************/

EXTRACT, SEQUENCE=psb4, FROM=PSB4.START, TO=BR4.BT_START, NEWNAME=psb4_ext;

psbbt4btm: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt4 + lbtm;
    psb4_ext        , AT =  0.0000000000 ;
	bt4          	, AT =  lpsbext ;
    btm          	, AT =  lpsbext + lbt4;
ENDSEQUENCE;

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

SEQEDIT, SEQUENCE = psbbt4btm;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR4.BT_START;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE4KFA14L1 := kBE4KFA14L1REF;

set, format="22.14e";
use, sequence= psbbt4btm;  
option, -warn;
save, sequence=psbbt4btm, beam, noexpr, file='jmad/psbbt4btm_dump.jmad';
option, warn;

set, format="22.14e";
use, sequence= bt4btm; 
option, -warn;
save, sequence=bt4btm, beam, noexpr, file='jmad/bt4btm_dump.jmad';
option, warn;

set, format="22.14e";
use, sequence= btm; 
option, -warn;
save, sequence=btm, beam, noexpr, file='jmad/btm_dump.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb4_start_lhc.inp jmad";
system, "cp bt4_start_lhc.inp jmad";
system, "cp btm4_start_lhc.inp jmad";

/*************************************
* Cleaning up
*************************************/

system, "rm bt_repo";
system, "rm btm_repo";
system, "rm bty_repo";
system, "rm -rf psb_repo || rm psb_repo";
stop;
