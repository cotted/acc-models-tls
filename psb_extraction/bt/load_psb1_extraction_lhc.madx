/******************************************************************
 **
 **  PSB extraction Ring 1: LHC
 **
 **  S. Ogur, W. Bartmann, M. Fraser and F. Velotti:
 **  Calculate initial conditions for BT extraction lines
 **
 ******************************************************************/

/******************************************************************
 * Call lattice files
 ******************************************************************/

system,"[ -d /afs/cern.ch/eng/acc-models/psb/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/psb/2021 psb_repo";
system,"[ ! -e psb_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-psb -b 2021 psb_repo";

option, -warn;

call, file = "psb_repo/psb.seq";
call, file = "psb_repo/psb_aperture.dbx";
call, file = "psb_repo/scenarios/lhc/2_flat_top/psb_ft_lhc.str";

/*******************************************************************************
 * Beam
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, sequence=psb1, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;
 
/******************************************************************
* Determine extraction location and install marker
******************************************************************/

! For debug purposes if needed
!use, sequence=PSB1;
!twiss,sequence=PSB1, file = 'psb_ring.tfs';

 /******************************************************************
 * Extraction bump
 ******************************************************************/
 
! Extraction bumper strengths
kBEBSW14L4 =  0.004403728 ;
kBEBSW15L1 = -0.005676606 ;
kBEBSW15L4 =  0.004412130 ;

! Redefine RBENDs as KICKERs (kick negative to outside) 
BE1.BSW14L4:  HKICKER, L=MDBAAWAP->L, KICK=-kBEBSW14L4;
BE1.BSW15L1:  HKICKER, L=MDBABWAP->L, KICK=-kBEBSW15L1;
BE1.BSW15L4:  HKICKER, L=MDBAAWAP->L, KICK=-kBEBSW15L4;

! Save initial conditions
use, sequence=PSB1;
savebeta, label=psbstart, PLACE=#s, SEQUENCE=PSB1;
twiss,sequence=PSB1; ! For debug purposes: file = 'psb_ring_kickers.tfs';

use, sequence=PSB1;
twiss,sequence=PSB1, beta0 = psbstart; ! For debug purposes: file = 'psb_tl_kickers.tfs';

 /******************************************************************
 * Prepare extraction sequence
 ******************************************************************/
 
! Install relevant markers 
BR1.BT_START  : MARKER; 
PSB1.START  : MARKER; 
seqedit, sequence = psb1;
flatten;
INSTALL, ELEMENT=PSB1.START, AT=0.0;
INSTALL, ELEMENT=BR1.BT_START, AT=0.639, FROM=BE1.SMH15L1;
flatten;
endedit;
                
! Turn on KFA14 (extraction kicker) and SMH15 (septum)
kBE1KFA14L1 = -0.007189859; ! negative angle to kick to  the right
kBESMH15L1 = -0.047; ! negative angle to kick to the right

! PSB extraction sequence
use, sequence=PSB1;
savebeta, label=btstart, PLACE=BR1.BT_START, SEQUENCE=PSB1;
twiss,sequence=PSB1, range = #s/BR1.BT_START, beta0 = psbstart; ! For debug purposes: file = 'psb_tl_extract.tfs';

calculate_extraction(delta_kicker, sign, ring_twiss_file) : macro = {
    
    if(sign == 1){
        kick_s = 1.0;
    }   
    else{
        kick_s = -1.0;
    };
    
    create,table=nominal, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
    create,table=trajectory, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
    select, flag = twiss, clear;
    savebeta,label=tl_initial_cond_nominal, place = BR1.BT_START;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;    
    use, sequence = PSB1;
    twiss, range = #s/BR1.BT_START, beta0 = psbstart, table=twiss_nom;
    
     /******************************************************************
     * Changes made here for kick repsonse
     ******************************************************************/
    kBE1KFA14L1REF = kBE1KFA14L1;
    kBE1KFA14L1 := kBE1KFA14L1REF + delta_kicker; 
    
    use, sequence = PSB1;

    select, flag = twiss, clear;
    savebeta,label=tl_initial_cond, place = BR1.BT_START;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
    twiss, range = #s/BR1.BT_START, beta0 = psbstart;

    len_twiss = table(twiss_nom, tablelength);
    value, len_twiss;
    
    i = 1;
    option, -info;
    while(i < len_twiss + 1){

        SETVARS, TABLE=twiss_nom, ROW=i;
        x = x;
        px = px;
        fill, table=nominal;
        SETVARS, TABLE=twiss, ROW=i;
        x = x;
        px = px;

        fill, table=trajectory;

        i = i + 1;
    };
    
    !write, table=nominal, file="ring_twiss_file_nominal.tfs";
    !write, table=trajectory, file="ring_twiss_file_kick_response.tfs";
    
};