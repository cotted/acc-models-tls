

  /**********************************************************************************
  *
  * BT version (draft) LS2 in MAD X SEQUENCE format
  * Generated the 17-JUL-2020 10:39:04 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

//---------------------- DRIFT          ---------------------------------------------
MSMIC002  : DRIFT       , L := 1.225;     ! BT Line Septum Magnet Injection - type C
MSMID     : DRIFT       , L := 1.224;     ! Septum Magnet Injection - type D
//---------------------- HKICKER        ---------------------------------------------
DHZ__8AF  : HKICKER     , L := .3;        ! DHZ Horizontal Dipole Corrector Magnet type 8af
//---------------------- INSTRUMENT     ---------------------------------------------
BTVBP018  : INSTRUMENT  , L := 0;         ! Beam TV Booster Type, Pneumatic, variant 018
BTVMB025  : INSTRUMENT  , L := 0;         ! Beam observation TV, with Magnetic coupling, type B
BTVMB038  : INSTRUMENT  , L := 0;         ! Beam observation TV, with Magnetic coupling, type B
BTVMB041  : INSTRUMENT  , L := 0;         ! Beam observation TV, with Magnetic coupling, type B
//---------------------- MARKER         ---------------------------------------------
OMK       : MARKER      , L := 0;         ! BT markers
//---------------------- MONITOR        ---------------------------------------------
BCTF_002  : MONITOR     , L := 0;         ! Fast Beam Current Transformers (Booster Ejection Lines)
BPUIA     : MONITOR     , L := 0;         ! Beam Position Pick-up Inductive type A
BPUIA002  : MONITOR     , L := 0;         ! Beam Position Pick-up Inductive type A. BT.BPM00s.
BPUIA004  : MONITOR     , L := 0;         ! Beam Position Pick-up Inductive type A. Shorter vacuum length
//---------------------- QUADRUPOLE     ---------------------------------------------
MQNCUNWP  : QUADRUPOLE  , L := .64;       ! Quadrupole magnet, type BT-BTP
//---------------------- SBEND          ---------------------------------------------
BVT__002  : SBEND       , L := 1.1317;    ! Vertical bending magnet. BT line
DVT__00T  : SBEND       , L := .403801904 ! DVT Vertical Dipole Corrector Magnet type T-DVT
MBHGAWWP  : SBEND       , L := 2;         ! Bending magnet, booster TL switching magnet
MBVBCWWP  : SBEND       , L := .906814;   ! Bending magnet, transfer line, type T-BV1 pulsed
MCVACWAP  : SBEND       , L := .348001241 ! Corrector magnet, dipole vertical, type T-DVT pulsed
MQNCRNWP  : SBEND       , L := .466;      ! Quadrupole magnet, type TRIUMF water, 0.43m
//---------------------- VKICKER        ---------------------------------------------
DVT__4AF  : VKICKER     , L := .436;      ! DVT Vertical Dipole Corrector Magnet type 4af
DVT__8AF  : VKICKER     , L := .335;      ! DVT Vertical Dipole Corrector Magnet type 8af
KFA__006  : VKICKER     , L := 0;         ! Recombination kickers


/************************************************************************************/
/*                       BT SEQUENCE                                                */
/************************************************************************************/

BT1 : SEQUENCE, refer = centre,    L = 33.917839;
 BT1.BVT10                     : MBVBCWWP     , at = 3.003045     , slot_id = 5554710;
 BT1.BPM00                     : BPUIA004     , at = 3.863195     , slot_id = 5554714;
 BT1.DHZ10                     : DHZ__8AF     , at = 4.46647      , slot_id = 5554718;
 BT1.VVS10                     : OMK          , at = 4.7236       , slot_id = 5554722;
 BT1.SMV10                     : MSMIC002     , at = 6.94073      , slot_id = 5554730;
 BT2.BPM10                     : BPUIA        , at = 8.730127     , slot_id = 5554741;
 BT2.QNO10                     : MQNCRNWP     , at = 9.373137     , slot_id = 5554743;
 BT2.VVS30                     : OMK          , at = 10.804132    , slot_id = 5554745;
 BT2.QNO20                     : MQNCRNWP     , at = 11.373291    , slot_id = 5554747;
 BT1.KFA10                     : KFA__006     , at = 14.7234      , slot_id = 5554752, assembly_id= 10430478;
 BT2.BVT20                     : BVT__002     , at = 16.624829    , slot_id = 5554756;
 BT2.BPM20                     : BPUIA        , at = 17.463502    , slot_id = 5554758;
 BT2.DVT40                     : DVT__8AF     , at = 18.930633    , slot_id = 5554759;
 BT2.SMV20                     : MSMID        , at = 20.955738    , slot_id = 5554765;
 BT.QNO30                      : MQNCRNWP     , at = 22.304301    , slot_id = 5554768;
 BT.BTV30                      : BTVMB041     , at = 23.008497    , slot_id = 53003985;
 BT.BPM30                      : BPUIA        , at = 25.763351    , slot_id = 5554769;
 BT.BCT10                      : BCTF_002     , at = 26.326859    , slot_id = 5554770;
 BT.DVT50                      : DVT__4AF     , at = 26.915868    , slot_id = 5554771;
 BT2.KFA20                     : KFA__006     , at = 29.684401    , slot_id = 5554774;
 BT.BTV40                      : BTVBP018     , at = 30.871905    , slot_id = 5554776, assembly_id= 5554774;
 BT.DVT60                      : DVT__4AF     , at = 31.239405    , slot_id = 5554777;
 BT.QNO40                      : MQNCUNWP     , at = 31.829404    , slot_id = 5554778;
 BT.BPM40                      : BPUIA        , at = 32.555404    , slot_id = 5554779;
 BT.QNO50                      : MQNCUNWP     , at = 33.199404    , slot_id = 5554780;
 BT.BHZ10                      : MBHGAWWP     , at = 34.981568    , slot_id = 5554781;
ENDSEQUENCE;

BT2 : SEQUENCE, refer = centre,    L = 33.90713;
 BT2.DVT10                     : MCVACWAP     , at = 2.802601     , slot_id = 5554711;
 BT2.BPM00                     : BPUIA002     , at = 3.901111     , slot_id = 5554715;
 BT2.DHZ10                     : DHZ__8AF     , at = 4.462617     , slot_id = 5554719;
 BT2.VVS10                     : OMK          , at = 4.8746       , slot_id = 5554723;
 BT2.DVT20                     : DVT__00T     , at = 5.658628     , slot_id = 5554726;
 BT2.SMV10                     : MSMIC002     , at = 6.930482     , slot_id = 12977914, assembly_id= 5554730;
 BT2.BTV10                     : BTVMB038     , at = 8.01488      , slot_id = 53004000;
 BT2.BPM10                     : BPUIA        , at = 8.71968      , slot_id = 5554741;
 BT2.QNO10                     : MQNCRNWP     , at = 9.362686     , slot_id = 5554743;
 BT2.VVS30                     : OMK          , at = 10.793581    , slot_id = 5554745;
 BT2.QNO20                     : MQNCRNWP     , at = 11.362701    , slot_id = 5554747;
 BT2.KFA10                     : KFA__006     , at = 14.712701    , slot_id = 14301505, assembly_id= 10430478;
 BT2.BTV20                     : BTVMB025     , at = 15.900201    , slot_id = 52941589, assembly_id= 10430478;
 BT2.BVT20                     : BVT__002     , at = 16.61412     , slot_id = 5554756;
 BT2.BPM20                     : BPUIA        , at = 17.452794    , slot_id = 5554758;
 BT2.DVT40                     : DVT__8AF     , at = 18.919925    , slot_id = 5554759;
 BT2.SMV20                     : MSMID        , at = 20.94503     , slot_id = 5554765;
 BT.QNO30                      : MQNCRNWP     , at = 22.293587    , slot_id = 5554768;
 BT.BTV30                      : BTVMB041     , at = 22.997789    , slot_id = 53003985;
 BT.BPM30                      : BPUIA        , at = 25.752643    , slot_id = 5554769;
 BT.BCT10                      : BCTF_002     , at = 26.316151    , slot_id = 5554770;
 BT.DVT50                      : DVT__4AF     , at = 26.905159    , slot_id = 5554771;
 BT2.KFA20                     : KFA__006     , at = 29.673692    , slot_id = 5554774;
 BT.BTV40                      : BTVBP018     , at = 30.861196    , slot_id = 5554776, assembly_id= 5554774;
 BT.DVT60                      : DVT__4AF     , at = 31.228696    , slot_id = 5554777;
 BT.QNO40                      : MQNCUNWP     , at = 31.818695    , slot_id = 5554778;
 BT.BPM40                      : BPUIA        , at = 32.544695    , slot_id = 5554779;
 BT.QNO50                      : MQNCUNWP     , at = 33.188695    , slot_id = 5554780;
 BT.BHZ10                      : MBHGAWWP     , at = 34.981568    , slot_id = 5554781;
ENDSEQUENCE;

BT3 : SEQUENCE, refer = centre,    L = 33.896157;
 BT3.DVT10                     : MCVACWAP     , at = 2.802601     , slot_id = 5554712;
 BT3.BPM00                     : BPUIA002     , at = 3.901111     , slot_id = 5554716;
 BT3.DHZ10                     : DHZ__8AF     , at = 4.462617     , slot_id = 5554720;
 BT3.VVS10                     : OMK          , at = 4.8746       , slot_id = 5554724;
 BT3.DVT20                     : DVT__00T     , at = 5.658628     , slot_id = 5554727;
 BT3.SMV10                     : MSMIC002     , at = 6.930482     , slot_id = 12977917, assembly_id= 5554731;
 BT3.BTV10                     : BTVMB038     , at = 8.01488      , slot_id = 53004015;
 BT3.BPM10                     : BPUIA        , at = 8.71968      , slot_id = 5554742;
 BT3.QNO10                     : MQNCRNWP     , at = 9.362686     , slot_id = 5557820;
 BT3.VVS20                     : OMK          , at = 9.999581     , slot_id = 5554744;
 BT3.VVS30                     : OMK          , at = 10.793581    , slot_id = 5554746;
 BT3.QNO20                     : MQNCRNWP     , at = 11.362701    , slot_id = 5557821;
 BT3.KFA10                     : KFA__006     , at = 14.712701    , slot_id = 14301508, assembly_id= 10430478;
 BT3.BTV20                     : BTVMB025     , at = 15.900201    , slot_id = 52941563, assembly_id= 10430478;
 BT3.DVT30                     : DVT__00T     , at = 16.362701    , slot_id = 5554755;
 BT3.BPM20                     : BPUIA        , at = 17.392705    , slot_id = 5554757;
 BT3.DVT40                     : DVT__00T     , at = 19.412611    , slot_id = 5554760;
 BT3.SMV20                     : MSMID        , at = 20.934518    , slot_id = 12978002, assembly_id= 5554765;
 BT.QNO30                      : MQNCRNWP     , at = 22.282722    , slot_id = 5554768;
 BT.BTV30                      : BTVMB041     , at = 22.98692     , slot_id = 53003985;
 BT.BPM30                      : BPUIA        , at = 25.741723    , slot_id = 5554769;
 BT.BCT10                      : BCTF_002     , at = 26.305223    , slot_id = 5554770;
 BT.DVT50                      : DVT__4AF     , at = 26.894223    , slot_id = 5554771;
 BT2.KFA20                     : KFA__006     , at = 29.662723    , slot_id = 5554774;
 BT.BTV40                      : BTVBP018     , at = 30.850223    , slot_id = 5554776, assembly_id= 5554774;
 BT.DVT60                      : DVT__4AF     , at = 31.217723    , slot_id = 5554777;
 BT.QNO40                      : MQNCUNWP     , at = 31.807722    , slot_id = 5554778;
 BT.BPM40                      : BPUIA        , at = 32.533722    , slot_id = 5554779;
 BT.QNO50                      : MQNCUNWP     , at = 33.177722    , slot_id = 5554780;
 BT.BHZ10                      : MBHGAWWP     , at = 34.981568    , slot_id = 5554781;
ENDSEQUENCE;

BT4 : SEQUENCE, refer = centre,    L = 33.906865;
 BT4.BVT10                     : MBVBCWWP     , at = 3.003045     , slot_id = 5554713;
 BT4.BPM00                     : BPUIA004     , at = 3.863195     , slot_id = 5554717;
 BT4.DHZ10                     : DHZ__8AF     , at = 4.46647      , slot_id = 5554721;
 BT4.VVS10                     : OMK          , at = 4.7236       , slot_id = 5554725;
 BT4.SMV10                     : MSMIC002     , at = 6.94073      , slot_id = 5554731;
 BT3.BPM10                     : BPUIA        , at = 8.730127     , slot_id = 5554742;
 BT3.QNO10                     : MQNCRNWP     , at = 9.373137     , slot_id = 5557820;
 BT3.VVS20                     : OMK          , at = 10.01007     , slot_id = 5554744;
 BT3.VVS30                     : OMK          , at = 10.804132    , slot_id = 5554746;
 BT3.QNO20                     : MQNCRNWP     , at = 11.373291    , slot_id = 5557821;
 BT4.KFA10                     : KFA__006     , at = 14.7234      , slot_id = 5554753, assembly_id= 10430478;
 BT3.DVT30                     : DVT__00T     , at = 16.37341     , slot_id = 5554755;
 BT3.BPM20                     : BPUIA        , at = 17.403413    , slot_id = 5554757;
 BT3.DVT40                     : DVT__00T     , at = 19.42332     , slot_id = 5554760;
 BT3.SMV20                     : MSMID        , at = 20.945226    , slot_id = 12978002, assembly_id= 5554765;
 BT.QNO30                      : MQNCRNWP     , at = 22.29343     , slot_id = 5554768;
 BT.BTV30                      : BTVMB041     , at = 22.997628    , slot_id = 53003985;
 BT.BPM30                      : BPUIA        , at = 25.752431    , slot_id = 5554769;
 BT.BCT10                      : BCTF_002     , at = 26.315931    , slot_id = 5554770;
 BT.DVT50                      : DVT__4AF     , at = 26.904931    , slot_id = 5554771;
 BT2.KFA20                     : KFA__006     , at = 29.673431    , slot_id = 5554774;
 BT.BTV40                      : BTVBP018     , at = 30.860931    , slot_id = 5554776, assembly_id= 5554774;
 BT.DVT60                      : DVT__4AF     , at = 31.228431    , slot_id = 5554777;
 BT.QNO40                      : MQNCUNWP     , at = 31.81843     , slot_id = 5554778;
 BT.BPM40                      : BPUIA        , at = 32.54443     , slot_id = 5554779;
 BT.QNO50                      : MQNCUNWP     , at = 33.18843     , slot_id = 5554780;
 BT.BHZ10                      : MBHGAWWP     , at = 34.981568    , slot_id = 5554781;
ENDSEQUENCE;

return;