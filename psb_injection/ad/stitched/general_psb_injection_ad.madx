/******************************************************************************************
 *
 * MAD-X input script for the injection optics of the AD cycle.
 *
 * 08/05/2020 - Fanouria Antoniou, Hannes Bartosik, Chiara Bracco, 
 * Gian Piero di Giovanni, Alexander Huschauer, Elisabeth Renner

 * 02/09/2020 - F.Velotti => fix for JMAD and proper PSB calling
 * 02/09/2020 - F.Velotti => saving sequence and check
 * 02/09/2020 - F.Velotti => groupping all BI stuff and dividing files using templates
 ******************************************************************************************/

make_ptctwiss_stitched(_ring_, _bunch_sett_, opt_name) : macro = {

    ptc_create_universe;
    ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
    SELECT, FLAG = ptc_twiss, CLEAR;
    select, flag=ptc_twiss, column=NAME,KEYWORD,ANGLE,K1L,S,L,BETX,BETY,DISP1,DISP3,X,Y,PX,PY,ALFX,ALFY,DISP2,DISP4,E1,E2,MU1,MU2,APER_1,APER_2;
    ptc_align;
    ptc_eplacement, range=bi_ring_.tstr1l1, onlyposition, x=xr0_ring_, y=yr0_ring_, z=0, refframe=previouselement, autoplacedownstream=true;
    ptc_twiss, icase=5,  no=5, table = ptc_twiss, summary_table = ptc_twiss_summary, normal,  beta0=initbeta0 , 
    file = twiss_bi_ring_psb_ring___bunch_sett_kev_opt_name.tfs;
    ptc_end;

};


system, "ln -nfs ./../.. main_dir";
call, file = "main_dir/templates/optics_convention.cmd";
call, file = "main_dir/templates/buncher_settings.cmd";

optics = ad;
buncher = kev_100;

call, file = "main_dir/templates/main_template.cmd";

! Just using this variable as numerical value
deb_value = db_set;

/************ Ring 1 ************/

call, file = "main_dir/templates/make_stitched_b1.cmd";

! Arguments = ring, debuncher settings,optics
exec, make_ptctwiss_stitched(1, $deb_value, ad);

! Argument = ring
exec, prepare_to_save(1);

! Arguments = optics name, debuncher settings, ring
exec, save_sequence(ad, $deb_value, 1);

/************ Ring 2 ************/
call, file = "main_dir/templates/make_stitched_b2.cmd";

exec, make_ptctwiss_stitched(2, $deb_value, ad);

! Argument = ring
exec, prepare_to_save(2);

! Arguments = optics name, debuncher settings, ring
exec, save_sequence(ad, $deb_value, 2);


/************ Ring 3 ************/

call, file = "main_dir/templates/make_stitched_b3.cmd";

exec, make_ptctwiss_stitched(3, $deb_value, ad);

! Argument = ring
exec, prepare_to_save(3);

! Arguments = optics name, debuncher settings, ring
exec, save_sequence(ad, $deb_value, 3);

if (deb_value == 100){

    write, table = ptc_twiss, file = "twiss_bi3psb3_100kev_ad_nom.tfs";

};

/************ Ring 4 ************/

call, file = "main_dir/templates/make_stitched_b4.cmd";

exec, make_ptctwiss_stitched(4, $deb_value, ad);

! Argument = ring
exec, prepare_to_save(4);

! Arguments = optics name, debuncher settings, ring
exec, save_sequence(ad, $deb_value, 4);

/************************************
* Cleaning up
************************************/
system, "rm psb_repo";
system, "rm internal_mag_pot.txt";
system, "rm main_dir";
system, "rm bi_repo";

stop;
