/******************************************************************
 * Ring 3
 ******************************************************************/


use, sequence = psb3;
seqedit, sequence=psb3;
 flatten;
  install, element= bi3.foil, class=marker, at = 0, from = bi3.tstr1l1;  
 flatten;
  cycle, start = bi3.foil;
 flatten;
endedit;

use, sequence = psb3;

seqedit, sequence = psb3;
    remove, element = bi3.tstr1l1;
endedit;
use, sequence = psb3;

/* exec, assign_KSW3_strength; */
/* exec, assign_BSW3_strength; */
/* exec, assign_BSW3_alignment; */


exec, ptc_twiss_macro(2,0,0);
xpsb03=-table(ptc_twiss,BI3.FOIL,X);


if (buncher == kev_100){
    call, file = "bi_repo/ini_cond_100kev.inp";
} elseif (buncher == kev_250){
    call, file = "bi_repo/ini_cond_250kev.inp";
} elseif (buncher == kev_450){
    call, file = "bi_repo/ini_cond_450kev.inp";
};

call, file = "bi_repo/bi_optics_r3.madx";
bi3psb3.seqlen = 157.08+ 48.49381;
BI3PSB3: SEQUENCE, refer = entry, L = bi3psb3.seqlen ;
bi3_foil, at=0;
psb3, at = 48.49381;
endsequence;

use, sequence = bi3psb3;

seqedit, sequence = bi3psb3;
    flatten;
endedit;
use, sequence = bi3psb3;

assign_errors_psb3(): macro = {
    /* exec, assign_KSW3_strength; */
    exec, bi3_macrobi3();
    /* exec, assign_BSW3_strength; */
    /* exec, assign_BSW3_alignment; */
};
