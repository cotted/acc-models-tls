ptc_twiss_macro(order, dp, slice_flag): macro={
  ptc_create_universe;
  ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
  !ptc_setswitch, debuglevel=0, nocavity=false, fringe=true, exact_mis=true, time=false, totalpath=true;
  !PTC_ALIGN; 
  IF (slice_flag == 1){
    select, flag=ptc_twiss, clear;
    select, flag=ptc_twiss, column=name,keyword,s,l,x,px,beta11,beta22,disp1,k1l;
    ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, slice_magnets=true;
  }
  ELSE{
    select, flag=ptc_twiss, clear;
    select, flag=ptc_twiss, column=name,keyword,s,x,px,beta11,alfa11,beta22,alfa22,disp1,disp2,disp3,disp4,mu1,mu2,energy,l,angle,K1L,K2L,K3L,HKICK,SLOT_ID;    
    ptc_twiss, closed_orbit, icase=56, no=order, deltap=dp, table=ptc_twiss, summary_table=ptc_twiss_summary, normal;
  }
  ptc_end;
};

write_ptc_twiss(filename) : macro = {
  write, table=ptc_twiss, file=filename;
};

assign_BSW1_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.1;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.4;
     EFCOMP, DKN:={+BSW_K0L, 0, +BSW_K2L};
     EPRINT;   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.3;
     EFCOMP, DKN:={-BSW_K0L, 0, -BSW_K2L}; 
     EPRINT;   
 };

 assign_BSW1_alignment: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.1;
     EALIGN, DX=-0.0057;

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.3;
     SELECT,FLAG=ERROR,RANGE=BI1.BSW1L1.4;
     EALIGN, DX=-0.0442;
 };

 assign_KSW1_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.KSW1L4;
     EFCOMP, DKN:={kLBIKSW1L4 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.KSW2L1;
     EFCOMP, DKN:={kLBIKSW2L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.KSW16L1;
     EFCOMP, DKN:={kLBIKSW16L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI1.KSW16L4;
     EFCOMP, DKN:={kLBIKSW16L4 * ksw_factor};   
 };


assign_BSW2_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.1;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.4;
     EFCOMP, DKN:={+BSW_K0L, 0, +BSW_K2L};
     EPRINT;   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.3;
     EFCOMP, DKN:={-BSW_K0L, 0, -BSW_K2L}; 
     EPRINT;   
 };

 assign_BSW2_alignment: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.1;
     EALIGN, DX=-0.0057;

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.3;
     SELECT,FLAG=ERROR,RANGE=BI2.BSW1L1.4;
     EALIGN, DX=-0.0442;
 };

 assign_KSW2_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.KSW1L4;
     EFCOMP, DKN:={kLBIKSW1L4 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.KSW2L1;
     EFCOMP, DKN:={kLBIKSW2L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.KSW16L1;
     EFCOMP, DKN:={kLBIKSW16L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI2.KSW16L4;
     EFCOMP, DKN:={kLBIKSW16L4 * ksw_factor};   
 };




assign_BSW3_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.1;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.4;
     EFCOMP, DKN:={+BSW_K0L, 0, +BSW_K2L};
     EPRINT;   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.3;
     EFCOMP, DKN:={-BSW_K0L, 0, -BSW_K2L}; 
     EPRINT;   
 };

 assign_BSW3_alignment: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.1;
     EALIGN, DX=-0.0057;

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.3;
     SELECT,FLAG=ERROR,RANGE=BI3.BSW1L1.4;
     EALIGN, DX=-0.0442;
 };

 assign_KSW3_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.KSW1L4;
     EFCOMP, DKN:={kLBIKSW1L4 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.KSW2L1;
     EFCOMP, DKN:={kLBIKSW2L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.KSW16L1;
     EFCOMP, DKN:={kLBIKSW16L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI3.KSW16L4;
     EFCOMP, DKN:={kLBIKSW16L4 * ksw_factor};   
 };



assign_BSW4_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.1;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.4;
     EFCOMP, DKN:={+BSW_K0L, 0, +BSW_K2L};
     EPRINT;   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.3;
     EFCOMP, DKN:={-BSW_K0L, 0, -BSW_K2L}; 
     EPRINT;   
 };

 assign_BSW4_alignment: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.1;
     EALIGN, DX=-0.0057;

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.2;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.3;
     SELECT,FLAG=ERROR,RANGE=BI4.BSW1L1.4;
     EALIGN, DX=-0.0442;
 };

 assign_KSW4_strength: macro = {
     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.KSW1L4;
     EFCOMP, DKN:={kLBIKSW1L4 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.KSW2L1;
     EFCOMP, DKN:={kLBIKSW2L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.KSW16L1;
     EFCOMP, DKN:={kLBIKSW16L1 * ksw_factor};   

     SELECT,FLAG=ERROR,CLEAR;
     SELECT,FLAG=ERROR,RANGE=BI4.KSW16L4;
     EFCOMP, DKN:={kLBIKSW16L4 * ksw_factor};   
 };


write_str_file(filename): macro = {
  assign, echo = filename;
  print, text = "/**********************************************************************************";
  print, text = "*                             MAIN QUADRUPOLES";
  print, text = "***********************************************************************************/";
  print, text = "";
  value, kQF, kQD;
  print, text = "";
  print, text = "/**********************************************************************************";
  print, text = "*                             INJECTION BUMP";
  print, text = "***********************************************************************************/";
  print, text = "";
  value, kLBIKSW1L4, kLBIKSW2L1, kLBIKSW16L1, kLBIKSW16L4, ksw_factor, BSW_K0L, BSW_K2L;
  !value, BSW_K0L, BSW_K2L;
  print, text = "";
  print, text = "/**********************************************************************************";
  print, text = "*                             BETA-BEATING CORRECTION";
  print, text = "***********************************************************************************/";
  print, text = "";
  value, kQD3CORR, kQD14CORR;
  assign, echo = terminal;
};


print, text = "/**********************************************************************************";
print, text = "*                             BI LINE ";
print, text = "***********************************************************************************/";

bi1_macrobi1(): macro = {
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT20";
  EFCOMP, order := 0, dkn := BI.angle.DVT20;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT30";
  EFCOMP, order := 0, dkn := BI.angle.DVT30;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT40";
  EFCOMP, order := 0, dkn := BI.angle.DVT40;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI1.DIS10.1";
  EFCOMP, order := 0, dkn := BI1.angle.DIS10.1;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI1.DIS10.2";
  EFCOMP, order := 0, dkn := BI1.angle.DIS10.2 ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI1.DIS10.3";
  EFCOMP, order := 0, dkn := BI1.angle.DIS10.3;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI1.DIS10.4";
  EFCOMP, order := 0, dkn := BI1.angle.DIS10.4;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI1.SMV10";
  EFCOMP, order := 0, dkn := BI1.angle.SMV10;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI1.BVT10";
  EFCOMP, order := 0, dkn := BI1.angle.BVT10*0.9785;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BRI1.BSW1L1.2";
  EFCOMP, order := 0, dkn := PSB.angle.BSW2;
  EPRINT;
  select, flag = error, clear;
};


bi2_macrobi2(): macro = {
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT20";
  EFCOMP, order := 0, dkn := BI.angle.DVT20;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT30";
  EFCOMP, order := 0, dkn := BI.angle.DVT30;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT40";
  EFCOMP, order := 0, dkn := BI.angle.DVT40;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI2.DIS10.1";
  EFCOMP, order := 0, dkn := BI2.angle.DIS10.1;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI2.DIS10.2";
  EFCOMP, order := 0, dkn := BI2.angle.DIS10.2;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI2.DIS10.3";
  EFCOMP, order := 0, dkn := BI2.angle.DIS10.3;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI2.DIS10.4";
  EFCOMP, order := 0, dkn := BI2.angle.DIS10.4;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI2.SMV10";
  EFCOMP, order := 0, dkn :=  BI2.angle.SMV10;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI2.BVT10";
  EFCOMP, order := 0, dkn := BI2.angle.BVT10*0.9865;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BRI2.BSW1L1.2";
  EFCOMP, order := 0, dkn := PSB.angle.BSW2;
  EPRINT;
  select, flag = error, clear;
};


bi3_macrobi3(): macro = {
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT20";
  EFCOMP, order := 0, dkn := BI.angle.DVT20;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT30";
  EFCOMP, order := 0, dkn := BI.angle.DVT30;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT40";
  EFCOMP, order := 0, dkn := BI.angle.DVT40;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI3.DIS10.1";
  EFCOMP, order := 0, dkn := BI3.angle.DIS10.1;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI3.DIS10.2";
  EFCOMP, order := 0, dkn := BI3.angle.DIS10.2 ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI3.DIS10.3";
  EFCOMP, order := 0, dkn :=  BI3.angle.DIS10.3 ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI3.DIS10.4";
  EFCOMP, order := 0, dkn :=  BI3.angle.DIS10.4;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BRI3.BSW1L1.2";
  EFCOMP, order := 0, dkn := PSB.angle.BSW2;
  EPRINT;
  select, flag = error, clear;
};

bi4_macrobi4(): macro = {
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT20";
  EFCOMP, order := 0, dkn := BI.angle.DVT20 ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT30";
  EFCOMP, order := 0, dkn := BI.angle.DVT30 ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI.DVT40";
  EFCOMP, order := 0, dkn := BI.angle.DVT40  ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI4.DIS10.1";
  EFCOMP, order := 0, dkn := BI4.angle.DIS10.1;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI4.DIS10.2";
  EFCOMP, order := 0, dkn :=  BI4.angle.DIS10.2;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI4.DIS10.3";
  EFCOMP, order := 0, dkn :=  BI4.angle.DIS10.3;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI4.DIS10.4";
  EFCOMP, order := 0, dkn :=  BI4.angle.DIS10.4;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI4.SMV10";
  EFCOMP, order := 0, dkn :=  BI4.angle.SMV10;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BI4.BVT10";
  EFCOMP, order := 0, dkn := BI4.angle.BVT10*0.9865 ;
  EPRINT;
  select, flag=error, clear;
  SELECT, FLAG=ERROR, RANGE="BRI4.BSW1L1.2";
  EFCOMP, order := 0, dkn := PSB.angle.BSW2;
  EPRINT;
  select, flag = error, clear;
};