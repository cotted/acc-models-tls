/******************************************************************************************
 *
 * MAD-X input script for the injection optics of the ISOLDE cycle.
 *
 * 08/05/2020 - Fanouria Antoniou, Hannes Bartosik, Chiara Bracco, 
 * Gian Piero di Giovanni, Alexander Huschauer, Elisabeth Renner

 * 02/09/2020 - F.Velotti => fix for JMAD and proper PSB calling
 * 02/09/2020 - F.Velotti => saving sequence and check
 * 02/09/2020 - F.Velotti => templates
 * 29/10/2020 - F.Velotti, C. Bracco, G.P. Di Giovanni => BI line definition moved to LINAC4 repo
 ******************************************************************************************/

 /******************************************************************
 * Energy and particle type definition
 ******************************************************************/
 
BEAM, PARTICLE=PROTON, PC = 0.571;
BRHO      := BEAM->PC * 3.3356;

set,  format="20.10f";

/******************************************************************
 * Cleaning .tfs output files
 ******************************************************************/

system, "rm *.tfs";

/******************************************************************
 * Call lattice files
 ******************************************************************/

system,"[ ! -e psb_repo ] && [ -d ./../../psb_repo ] && ln -nfs ./../../psb_repo psb_repo";
system,"[ ! -e psb_repo ] && [ -d /afs/cern.ch/eng/acc-models/psb/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/psb/2021 psb_repo";
system,"[ ! -e psb_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-psb -b 2021 psb_repo";

system,"[ ! -e linac_repo ] && [ -d ./../../../../acc-models-linac4 ] && ln -nfs ./../../../../acc-models-linac4 linac_repo";
system,"[ ! -e linac_repo ] && [ -d /afs/cern.ch/eng/acc-models/linac4/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/linac4/2021 linac_repo";
system,"[ ! -e linac_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-linac4 -b 2021 linac_repo";

system, "ln -nfs linac_repo/elements/bi bi_repo";

call, file = "psb_repo/psb.seq";
call, file = "psb_repo/psb_aperture.dbx";

if (optics == lhc){
    call, file = "psb_repo/scenarios/lhc/0_injection/psb_inj_lhc.str";
}elseif (optics == ad){
    call, file = "psb_repo/scenarios/ad/0_injection/psb_inj_ad.str";

} elseif (optics == tof){
    call, file = "psb_repo/scenarios/tof/0_injection/psb_inj_tof.str";

} elseif (optics == sftpro){
    call, file = "psb_repo/scenarios/sftpro/0_injection/psb_inj_sftpro.str";

} elseif (optics == isolde){
    call, file = "psb_repo/scenarios/isolde/0_injection/psb_inj_isolde.str";

} elseif (optics == east){
    call, file = "psb_repo/scenarios/east/0_injection/psb_inj_east.str";

};

call, file = "main_dir/templates/macros.cmd";
call, file = "bi_repo/bi.seq" ;

if (buncher == kev_100){
    call, file="linac_repo/scenarios/nominal/debuncher_plus650kV/nominal_plus650kV.str";
    call, file = "bi_repo/bi100kev.str";
    db_set = 100;
} elseif (buncher == kev_250){
    call, file="linac_repo/scenarios/nominal/debuncher_minus100kV/nominal_minus100kV.str";
    call, file = "bi_repo/bi250kev.str";
    db_set = 250;
} elseif (buncher == kev_450){
    call, file="linac_repo/scenarios/nominal/debuncher_minus750kV/nominal_minus750kV.str";
    call, file = "bi_repo/bi450kev.str";
    db_set = 450;
};


make_l4t_2_ltb_sequence(): macro = {
    
    option, -warn;
    call, file = 'linac_repo/elements/l4t.seq';
    call, file = 'linac_repo/elements/l4z.seq';
    call, file = 'linac_repo/elements/lt.seq';
    call, file = 'linac_repo/elements/ltb.seq';
    call, file = 'linac_repo/elements/l4t.dbx';
    call, file = 'linac_repo/elements/lt.dbx';
    call, file = 'linac_repo/elements/ltb.dbx';
    call, file = 'linac_repo/elements/lbe.seq';
    option, warn;

    use, sequence=l4t;
    use, sequence=l4z;

    use, sequence=lt;
    use, sequence=ltb;

    l4tltltb: sequence, refer=ENTRY,  l = L4T.seqlen+LT.seqlen+LTB.seqlen ;
      l4t                         , at = 0;
      lt                          , at = L4T.seqlen;
      ltb                         , at = L4T.seqlen + LT.seqlen ;
    endsequence;

    use, sequence = lbe;
    l4tltltblbe: sequence, refer=ENTRY,  l = L4T.seqlen+LT.seqlen+LTB.seqlen+LBE.seqlen ;
        l4t                         , at = 0;
        lt                          , at = L4T.seqlen;
        ltb                         , at = L4T.seqlen + LT.seqlen ;
        lbe                         , at = L4T.seqlen + LT.seqlen + LTB.seqlen ;
    endsequence;


};

make_l4t_2_psb(__ring__): macro = {

    l4tltltbbi__ring__psb__ring__: sequence, refer=ENTRY,  l = L4T.seqlen+LT.seqlen+LTB.seqlen + bi__ring__psb__ring__.seqlen ;
      l4t                         , at = 0;
      lt                          , at = L4T.seqlen;
      ltb                         , at = L4T.seqlen + LT.seqlen ;
      bi__ring__psb__ring__       , at = L4T.seqlen + LT.seqlen + ltb.seqlen ;
    endsequence;

    use, sequence = l4tltltbbi__ring__psb__ring__;
    seqedit, sequence = l4tltltbbi__ring__psb__ring__;
        flatten;
    endedit;
    use, sequence = l4tltltbbi__ring__psb__ring__;

};


save_sequence(opt_name, bunch_sett, _ring_) : macro = {
    option, -warn;
    save, sequence = bi_ring_psb_ring_, 
    file="./jmad/bi_ring_psb_ring__bunch_settkev_opt_name_savedseq.seq", beam;
    option, warn;

}

save_sequence_glued(opt_name, bunch_sett, _ring_) : macro = {
    option, -warn;
    save, sequence = l4tltltbbi_ring_psb_ring_, 
    file="./jmad/l4t_lt_ltb_bi_ring_psb_ring__bunch_settkev_opt_name_savedseq.seq", beam;
    option, warn;

}

save_sequence_lbe(__bunch_sett__): macro = {
    use, sequence = l4tltltblbe;
    option, -warn;
    save, sequence = l4tltltblbe,
    file="./jmad/lbe_seq___bunch_sett__kev_opt_savedseq.seq", beam;
    option, warn;
};

prepare_to_save(_beam_) : macro = {

    ! Saving all errors to file
    SELECT, FLAG = ERROR, FULL;

    if (buncher == kev_100){
        ESAVE, FILE = "./jmad/bi_beam_psb_beam__100kev_errors.seq";
        assign, echo="./jmad/bi_beam_psb_beam__100kev_ref_change.seq";
    } elseif (buncher == kev_250){
        esave, file = "./jmad/bi_beam_psb_beam__250kev_errors.seq";
        assign, echo="./jmad/bi_beam_psb_beam__250kev_ref_change.seq";
    } elseif (buncher == kev_450){
        esave, file = "./jmad/bi_beam_psb_beam__450kev_errors.seq";
        assign, echo="./jmad/bi_beam_psb_beam__450kev_ref_change.seq";
    };

    print, text="/*********************************************************************";
    print, text='Values to fix reference sytstem';
    print, text="*********************************************************************/";

    print, text = '';

    value, xr0_beam_;
    value, yr0_beam_;

    assign, echo=terminal;

};

prepare_to_save_glued(_beam_) : macro = {

    ! Saving all errors to file
    SELECT, FLAG = ERROR, FULL;

    if (buncher == kev_100){
        ESAVE, FILE = "./jmad/l4t_lt_ltb_bi_beam_psb_beam__100kev_errors.seq";
        assign, echo= "./jmad/l4t_lt_ltb_bi_beam_psb_beam__100kev_ref_change.seq";
    } elseif (buncher == kev_250){
        esave, file = "./jmad/l4t_lt_ltb_bi_beam_psb_beam__250kev_errors.seq";
        assign, echo="./jmad/l4t_lt_ltb_bi_beam_psb_beam__250kev_ref_change.seq";
    } elseif (buncher == kev_450){
        esave, file = "./jmad/l4t_lt_ltb_bi_beam_psb_beam__450kev_errors.seq";
        assign, echo="./jmad/l4t_lt_ltb_bi_beam_psb_beam__450kev_ref_change.seq";
    };

    print, text="/*********************************************************************";
    print, text='Values to fix reference sytstem';
    print, text="*********************************************************************/";

    print, text = '';

    value, xr0_beam_;
    value, yr0_beam_;

    assign, echo=terminal;

};
