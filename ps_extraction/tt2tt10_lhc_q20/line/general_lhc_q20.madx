/*****************************************************
 * MADX optics file for LHC Q20
 * Based on files from AFS repository 
 *
 * F.Velotti
 *****************************************************/
 title, "TT2/TT10 optics. Protons - 26 GeV/c";

 option, echo;
 option, RBARC=FALSE;  ! the length of a rectangular magnet
                       ! is the distance between the polefaces
                       ! and not the arc length

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt2 tt2_repo";
system, "ln -fns ./../../tt10 tt10_repo";

/*******************************************************************************
 * Beam
 *******************************************************************************/
Beam, particle=PROTON,pc=26,exn=3.5e-6,eyn=3.5E-6;
BRHO      := BEAM->PC * 3.3356;

/*****************************************************************************
 * TT2
 *****************************************************************************/
 option, -echo;
 call, file = "./tt2_repo/tt2.ele";
 call, file = "./tt2_fe_26.str";
 call, file = "./tt2_repo/tt2.seq";
 call, file = "./tt2_repo/tt2.dbx";

 option, echo;


/*******************************************************************************
 * TT10
 *******************************************************************************/

 call, file = "./tt10_repo/tt10.ele";
 call, file = "./tt10_fe_26_q20.str";
 call, file = "./tt10_repo/tt10.seq";
 call, file = "./tt10_repo/tt10.dbx";


/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/

 tt2tt10: sequence, refer=ENTRY, l = 1164.8409;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  tt10                  , at = 304.6954;
 endsequence;

/*******************************************************************************
 * set initial twiss parameters
 *******************************************************************************/
! Those should be the correct initial conditions as obtained from the PS
call, file = "./../stitched/tt2_tt10_lhc_q20_from_stitched_kickers.inp";

! Hystorical conditions and kept only until re-matching done
!call, file = "./../stitched/tt2_tt10_lhc_q20_old.inp";

/*******************************************************************************
 * set initial position and angle (x,px)
 * E.g. :
 * x0 := 0.0;
 * px0:= 0.0;
 *******************************************************************************/


/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;



/*******************************************************************************
 * Twiss
 *******************************************************************************/

select, flag = twiss, clear;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;

use, sequence=tt2tt10;

twiss, beta0=initbeta0, centre, file = "twiss_tt2_tt10_lhc_q20_nom.tfs";

/*******************************************************************
* Saving sequence for JMAD
*******************************************************************/

system, "rm ./jmad/*";
system, "mkdir jmad";

set, format="10.8f";
option, -warn;
save, sequence = tt2tt10, file="./jmad/tt2_lhc_q20_savedseq.seq", beam;
option, warn;


/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="./jmad/tt2_lhc_q20.inp";

betx0 = initbeta0->betx;
bety0 = initbeta0->bety;
alfx0 = initbeta0->alfx;
alfy0 = initbeta0->alfy;
dx0   = initbeta0->dx;
dy0   = initbeta0->dy;
dpx0  = initbeta0->dpx;
dpy0  = initbeta0->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;
/************************************
* Cleaning up
************************************/
system, "rm tt2_repo";
system, "rm tt10_repo";
stop;

