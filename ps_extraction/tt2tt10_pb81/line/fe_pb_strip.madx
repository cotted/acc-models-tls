
/*******************************************************************************
 *******************************************************************************
 * MADX file for TT2/TT10 optics calculations
 * This file is for LHC Ions with a momentum per nucleon of 6.75 GeV/c/u
 *
 * The ions extracted from the PS have a charge state 54+
 * and have the same magnetic rigidity as a proton beam at 26 GeV/c
 * The ions are stripped in TT2 to 82+
 *
 *
 * Execute with:  >madx < fe_pb_strip.madx
 *
 *
 * This MADX file compute the optics for the ions, by dividing the TT2-TT10 line in two parts.
 *  1) Twiss computation from the beginning of TT2 to the stripper (STRN : marker;).
 *  2) The Coulomb scattering at the stripping foil and its effect on the Twiss parameter
 *     are taken into account. The modified Twiss parameters (after the stripping foil) are used
 *     as initial conditions for the second part of the line, from STRN to the end of TT10.
 * (G.Arduini, E.Benedetto, Oct 2007)
 *******************************************************************************
 *******************************************************************************/


 option, echo;
!option, double;
 option, RBARC=FALSE;
!assign, echo="echo.prt";



/*******************************************************************************
 * TITLE
 *******************************************************************************/
 title, 'TT2/TT10 optics. LHC Ion beam 54+/82+, 6.75 GeV/c/u ~ 26 GeV/c proton beam';



/*******************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *     The reason is a >feature< of MADX
 *******************************************************************************/
 option, -echo;
 call, file = '../elements/tt2.ele';
 call, file = '../strength/tt2_fe_pb_strip.str';
 call, file = '../sequence/tt2.seq';
 call, file = '../aperture/tt2.dbx';
 option, echo;



/*******************************************************************************
 * TT10
 * NB! The order of the .ele .str and .seq files matter.
 *     The reason is a >feature< of MADX
 *******************************************************************************/
 option, -echo;
 call, file = '../elements/tt10.ele';
 call, file = '../strength/tt10_fe_pb_strip.str';
 call, file = '../sequence/tt10.seq';
 call, file = '../aperture/tt10.dbx';
 option, echo;



/*******************************************************************************
 * SPS
 *******************************************************************************/

 option, -echo;
 call, file = '/afs/cern.ch/eng/sps/2014
               /elements/sps.ele';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_1.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_2.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /sequence/sps.seq';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_3.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /strength/ft_noqs_ext.str';
 call, file = '/afs/cern.ch/eng/sps/2014
               /strength/elements.str';
 option, echo;



/*******************************************************************************
 * Junction between TT10 and SPS
 *******************************************************************************/

 SEQEDIT, SEQUENCE=SPS;
 ENDTT10 : MARKER;
 install, element = ENDTT10, at = -0.1375, from = BPH.12008;
 ENDEDIT;

 SEQEDIT, SEQUENCE=SPS;
 ENDVKNV : MARKER;
 install, element = ENDVKNV, at = 10.3015+0.1755, from = BPCE.11833;
 ENDEDIT;

 SEQEDIT, SEQUENCE=SPS;
 flatten ; cycle, start=  ENDVKNV;
 ENDEDIT;



/*******************************************************************************
 * Build up the geometry of the beam lines and select a line
 *******************************************************************************/

tt2:        sequence, refer=ENTRY, l = 304.6954;
           tt2a                  , at =        0;
           tt2b                  , at = 136.3114;
           tt2c                  , at = 249.9449;
endsequence;

tt2tt10:    sequence, refer=ENTRY, l = 1164.8409;
           tt2a                  , at =         0;
           tt2b                  , at =  136.3114;
           tt2c                  , at =  249.9449;
           tt10                  , at =  304.6954;
endsequence;

tt2tt10sps: sequence, refer=ENTRY, l = 8076.3447;
           tt2a                  , at =         0;
           tt2b                  , at =  136.3114;
           tt2c                  , at =  249.9449;
           tt10                  , at =  304.6954;
           sps                   , at = 1164.8409;
endsequence;



/*******************************************************************************
 * Set initial twiss parameters. Start of TT2
 *******************************************************************************/
call, file = '../inp/tt2_fe_pb_strip.inp';



/*******************************************************************************
 * set initial position and angle (x,px)
 * E.g. :
 * x0 := 0.0;
 * px0:= 0.0;
 *******************************************************************************/



/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;



/*******************************************************************************
 * save a sequence
 *******************************************************************************/
 !save, sequence=tt2, file=tt2.save;



/*******************************************************************************
 * maketwiss macro
 *******************************************************************************/
 maketwiss : macro={
                    ptc_create_universe;
                    ptc_create_layout,model=2,method=6,nst=5,time=true,exact;
                    ptc_twiss,table=ptc_twiss,BETA0=INITBETA0,DELTAP=0,icase=5,no=1; ! , file="twiss"
                    ptc_end;
                   }



/*******************************************************************************
 *******************************************************************************
 * Set up beam parameters at the start of TT2
 *
 * Only the beam parameters after the stripper were measured; The beam parameters
 * before the stripper are evaluated after calculating the optics
 *******************************************************************************
 *******************************************************************************/
! amu  = 0.9315; ! [GeV/c^2], http://en.wikipedia.org/wiki/Atomic_mass_unit
A      = 208;    ! sum of protons and neutrons
Beam, particle=lead,pc=6.75*A,charge=54, mass=A*0.9315, BUNCHED; !(no emittance defined yet)
use, period=tt2, range=#s/strn;



/***************************************************
 * TWISS PTC. Start of TT2
 ***************************************************/
 exec, maketwiss;



/***************************************************
 * store optical parameters @ STRN in memory block: STRIPPERENTRY
 ***************************************************/
STRIPPERENTRY: BETA0,
  BETX=table(ptc_twiss,STRN,beta11),
  ALFX=table(ptc_twiss,STRN,alfa11),
  MUX=table(ptc_twiss,STRN,mu1),
  BETY=table(ptc_twiss,STRN,beta22),
  ALFY=table(ptc_twiss,STRN,alfa22),
  MUY=table(ptc_twiss,STRN,mu2),
  X=table(ptc_twiss,STRN,x),
  PX=table(ptc_twiss,STRN,px),
  Y=table(ptc_twiss,STRN,y),
  PY=table(ptc_twiss,STRN,py),
  T=0,
  PT=0,
  DX=table(ptc_twiss,STRN,disp1),
  DPX=table(ptc_twiss,STRN,disp2),
  DY=table(ptc_twiss,STRN,disp3),
  DPY=table(ptc_twiss,STRN,disp4);

value, STRIPPERENTRY->DX, STRIPPERENTRY->DPX;
value, STRIPPERENTRY->X, STRIPPERENTRY->PX;
! From this I get: X=-0.01395031, PX= -0.00050768;
!show, STRIPPERENTRY;



/***************************************************
 * store optical parameters @ STRN in memory block: BEGTT2
 ***************************************************/
BEGTT2: BETA0,
  BETX=table(ptc_twiss,BEGTT2A,beta11),
  ALFX=table(ptc_twiss,BEGTT2A,alfa11),
  MUX=table(ptc_twiss,BEGTT2A,mu1),
  BETY=table(ptc_twiss,BEGTT2A,beta22),
  ALFY=table(ptc_twiss,BEGTT2A,alfa22),
  MUY=table(ptc_twiss,BEGTT2A,mu2),
  X=table(ptc_twiss,BEGTT2A,x),
  PX=table(ptc_twiss,BEGTT2A,px),
  Y=table(ptc_twiss,BEGTT2A,y),
  PY=table(ptc_twiss,BEGTT2A,py),
  T=0,
  PT=0,
  DX=table(ptc_twiss,BEGTT2A,disp1),
  DPX=table(ptc_twiss,BEGTT2A,disp2),
  DY=table(ptc_twiss,BEGTT2A,disp3),
  DPY=table(ptc_twiss,BEGTT2A,disp4);





/***************************************************
 * Evaluation of the beam parameters before the stripper.
 ***************************************************/
! NB. The values dpp=0.55e-3,exn=0.58um and eyn=0.54um were measured after the stripper.
!     The design parameters are dpp=0.55e-3, exn=1.0um and eyn=1.0um after the stripper.

!exnafterstripper=1.0e-6; ! norm. emittance after the stripper - HOR
!eynafterstripper=1.0e-6; ! norm. emittance after the stripper - VER

exnafterstripper=0.58e-6; ! norm. emittance after the stripper - HOR
eynafterstripper=0.54e-6; ! norm. emittance after the stripper - VER


!   In order to estimate the emittance before the stripper, we need to evaluate:
!    1) the blow-up due to the stripper
!    2) the twiss parameters at the stripper

dpp    = 5.5e-4; ! energy spread
ZSCAT  = 82;     ! The scattering is calculated assuming
                 ! that the ions loose all the electrons
                 ! immediately at the entrance of the stripper.
L      = 0.8e-3; ! stripper thickness [m]
csi0   = 8.9e-2; ! X0 = Radiation length of aluminium (89mm)

thetascatt_rms = 13.6 * ZSCAT/(beam->beta*beam->pc*1000) * sqrt(L/csi0) * (1+0.038*log(L/csi0));

value, thetascatt_rms;! Scattering angle, caused by the stripper
value, beam->beta, beam->pc, sqrt(L/csi0);

delta_epsx     = 0.5*stripperentry->betx*thetascatt_rms^2; ! emittance blow-up- HOR
delta_epsy     = 0.5*stripperentry->bety*thetascatt_rms^2; ! emittance blow-up- VER

exnbefstripper = exnafterstripper-delta_epsx*beam->beta*beam->gamma; ! norm. emittance before stripper - HOR
eynbefstripper = eynafterstripper-delta_epsy*beam->beta*beam->gamma; ! norm. emittance before stripper - VER

exbefstripper = exnbefstripper/(beam->beta*beam->gamma);  ! emittance before the stripper - HOR
eybefstripper = eynbefstripper/(beam->beta*beam->gamma);  ! emittance before the stripper - VER






/*******************************************************************************
 *******************************************************************************
 **  TT2[#S] - TT2[STRN]
 **  From the start of TT2 to the stripper
 *******************************************************************************
 *******************************************************************************/

! NB!    The reason that the emittance is mutiplied by 4 i.e. exn=4*,  is a MAD definition
!        54 is the charge of the lead ions before stripping.
 A      = 208;    ! sum of protons and neutrons
 Beam, particle=lead, pc=6.75*A
     , exn=exnbefstripper*4.0, eyn=eynbefstripper*4.0
     , charge=54, mass=A*0.9315
     , sige=dpp/(BEAM->ENERGY/BEAM->PC)^2
     , BUNCHED;

 show, beam;


/***************************************************
 * Set initial twiss parameters. Start of TT2
 ***************************************************/
 BETX0         = BEGTT2->betx;
 BETY0         = BEGTT2->bety;
 ALFX0         = BEGTT2->alfx;
 ALFY0         = BEGTT2->alfy;
 DX0           = BEGTT2->dx;
 DPX0          = BEGTT2->dpx;
 DY0           = BEGTT2->dy;
 DPY0          = BEGTT2->dpy;

 show, begtt2;



/***************************************************
 * store initial parameters in memory block
 ***************************************************/
 INITBETA0: BETA0,
   BETX=BETX0,
   ALFX=ALFX0,
   MUX=MUX0,
   BETY=BETY0,
   ALFY=ALFY0,
   MUY=MUY0,
   X=X0,
   PX=PX0,
   Y=Y0,
   PY=PY0,
   T=T0,
   PT=PT0,
   DX=DX0,
   DPX=DPX0,
   DY=DY0,
   DPY=DPY0;



/***************************************************
 * TWISS @ From start of TT2
 ***************************************************/

 use, period=tt2tt10sps, range=#s/strn;
 exec, maketwiss;

!------------------------ write ptc_twiss table -----------------
 set,  format="10.5f"; ! the format of numbers in the twiss output file

 select,flag=ptc_twiss, clear;
 select,flag=ptc_twiss, range=#S/#E;
 select,flag=ptc_twiss, column = name,angle,k1l,k2l,k3l,beta11,beta22,disp1,disp3,x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;

 write,table=ptc_twiss, file="opticsxfepbstrip1.out";
!----------------------------------------------------------------

!write,table=ptc_twiss;





 /*******************************************************************************
 *******************************************************************************
 **  TT2[STRN] - TT10
 **  From the the stripper in TT2 to SPS
 *******************************************************************************
 *******************************************************************************/


/*******************************************************************************
 * Set up beam parameters after the stripper
 * 82 is the charge of the lead ions after stripping.
 *******************************************************************************/
 A      = 208;    ! sum of protons and neutrons
 Beam, particle=lead, pc=6.75*A
     , exn=exnafterstripper*4.0, eyn=eynafterstripper*4.0
     , charge=82, mass=A*0.9315
     , sige=dpp/(BEAM->ENERGY/BEAM->PC)^2
     , BUNCHED;



/*******************************************************************************
 * Set initial twiss parameters. At the stripper in TT2
 *******************************************************************************/
 BETX0         = (stripperentry->betx*exbefstripper+L^2/3*thetascatt_rms^2)/(exbefstripper+delta_epsx);
 BETY0         = (stripperentry->bety*eybefstripper+L^2/3*thetascatt_rms^2)/(eybefstripper+delta_epsy);
 ALFX0         = (stripperentry->alfx*exbefstripper-0.5*L*thetascatt_rms^2)/(exbefstripper+delta_epsx);
 ALFY0         = (stripperentry->alfy*eybefstripper-0.5*L*thetascatt_rms^2)/(eybefstripper+delta_epsy);
 DX0           =  stripperentry->dx;
 DPX0          =  stripperentry->dpx;
 DY0           =  stripperentry->dy;
 DPY0          =  stripperentry->dpy;

 value, betx0,bety0,alfx0,alfy0,dx0,dy0,dpx0,dpy0,exbefstripper,eybefstripper,beam->ex,beam->ey;
 show, stripperentry;
 show, beam;
 show, begtt2;



/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;



/*******************************************************************************
 * TWISS @ From the stripper in TT2 to SPS
 *******************************************************************************/
 use, period=tt2tt10sps, range=strn/#e;
 exec, maketwiss;

!------------------------ write ptc_twiss table -----------------
set,  format="10.5f"; ! the format of numbers in the twiss output file

select,flag=ptc_twiss, clear;
select,flag=ptc_twiss, range=#S/#E;
select,flag=ptc_twiss, column = name,s,l,angle,k11,x,px,disp1,disp2,alfa11,beta11,mu1,
                                                   y,py,disp3,disp4,alfa22,beta22,mu2;
write,table=ptc_twiss, file="opticsxfepbstrip2.out";
!----------------------------------------------------------------

!write,table=ptc_twiss;






/*******************************************************************************
 * Comments
 *******************************************************************************/
print, text="The file: echo.prt is on echo.prt";



stop;

