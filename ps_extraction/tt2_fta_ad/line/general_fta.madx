!==============================================================================================
! MADX file for FTA optics
!
! F.M. Velotti, M.A. Fraser
! based on version from AFS repository from O. Berrig and E. Benedetto (2007)
!
! Changelog: Extraction elements included from PS and taken into account to
! calculate initial conditions and stitched model as done for nTOF
! Compared with measurements taken in 2018 by M. Fraser
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT2/FTA optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt2 tt2_repo";


/*****************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "tt2_repo/tt2.ele";
 call, file = "tt2_fe_26_ad.str";
 call, file = "tt2_repo/tt2.seq";
 call, file = "tt2_repo/tt2.dbx";
 option, echo;



/*******************************************************************************
 * FTA line
 *******************************************************************************/
 call, file = "fta.ele";
 call, file = "fta_fe_26_k.str";
 call, file = "fta.seq";
 call, file = "fta.dbx";



/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 tt2fta: sequence, refer=ENTRY, l = 136.3114+60.145965;
   tt2a                  , at =        0;
   fta                   , at = 136.3114;
  endsequence;



/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./../stitched/tt2_ad_from_stitched_kickers.inp";



/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * beam
 *******************************************************************************/
 Beam, particle=PROTON,pc=26,exn=10.0E-6,eyn=5.0E-6;
 use, sequence= tt2fta;


/*******************************************************************************
 * MATCH
 *******************************************************************************/
/*****************************************************
 * MATCH with currents 
 *****************************************************
 MATCH , sequence=tt2fta
       , BETx   = BETX0,    ALFx = ALFX0,   MUx  = MUX0
       , X      = X0   ,    Px   = PX0
       , Dx     = DX0  ,    Dpx  = DPX0
       , BETy   = BETY0,    ALFy = ALFY0,   MUy  = MUY0
       , Y      = Y0   ,    Py   = PY0
       , Dy     = DY0  ,    Dpy  = DPY0
       , deltap = 0.0  ;

       !IQDE9010 := 0
       !IQFO9020 := 0
       !IQDE9030 := 0
       !IQFO9040 := 0
       !IQFO9050 := 0
       !IQDE9052 := 0
       !vary , NAME=IQDE9010, STEP =  1.0  , LOWER =  170.0, UPPER =  230.0;
       !vary , NAME=IQFO9020, STEP =  1.0  , LOWER =  170.0, UPPER =  230.0;
       !vary , NAME=IQDE9030, STEP =  1.0  , LOWER =  250.0, UPPER =  350.0;
        vary , NAME=IQFO9040, STEP = 0.0001, LOWER =  250.0, UPPER =  400.0;
        vary , NAME=IQFO9050, STEP = 10.0  , LOWER = 1500.0, UPPER = 3000.0;
        vary , NAME=IQDE9052, STEP = 10.0  , LOWER = 1500.0, UPPER = 2900.0;
        constraint , pattern= TARGMA, BetX=0.50, BetY=0.50;
        constraint , pattern= TARGMA, DX= 0.20 , DY= 0.20;
        weight , BetX=99., BetY=99., DX=99., DY=99.0;
        simplex, calls = 5000 , tolerance = 1.d-5;
        migrad , calls = 5000 , tolerance = 1.d-5;
 ENDMATCH;
 *****************************************************/


/*****************************************************
 * MATCH with strengths
 *****************************************************
 MATCH , sequence=tt2fta
       , BETx   = BETX0,    ALFx = ALFX0,   MUx  = MUX0
       , X      = X0   ,    Px   = PX0
       , Dx     = DX0  ,    Dpx  = DPX0
       , BETy   = BETY0,    ALFy = ALFY0,   MUy  = MUY0
       , Y      = Y0   ,    Py   = PY0
       , Dy     = DY0  ,    Dpy  = DPY0
       , deltap = 0.0  ;

       !IQDE9010 := 0
       !IQFO9020 := 0
       !IQDE9030 := 0
       !IQFO9040 := 0
       !IQFO9050 := 0
       !IQDE9052 := 0
       !vary , NAME=KQDE9010;
       !vary , NAME=KQFO9020;
       !vary , NAME=KQDE9030;
        vary , NAME=KQFO9040;
        vary , NAME=KQFO9050;
        vary , NAME=KQDE9052;
        constraint , pattern= TARGMA, BetX=0.50, BetY=0.50;
        constraint , pattern= TARGMA, DX= 0.20 , DY= 0.20;
        weight , BetX=99., BetY=99., DX=99., DY=99.0;
        simplex, calls = 5000 , tolerance = 1.d-5;
        migrad , calls = 5000 , tolerance = 1.d-5;
 ENDMATCH;
 *****************************************************/


value, QDE9010->K1;
value, QFO9020->K1;
value, QDE9030->K1;
value, QFO9040->K1;
value, QFO9050->K1;
value, QDE9052->K1;



/*******************************************************************************
 * twiss
 *******************************************************************************/

SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_tt2_fta_nom.tfs";


/*************************************
* Survey
*************************************/
call, file = "./make_survey.madx";

/*******************************************************************
* Saving sequence for JMAD
*******************************************************************/

system, "rm ./jmad/*";
system, "mkdir jmad";

set, format="10.8f";
option, -warn;
save, sequence = tt2fta, file="./jmad/tt2_fta_savedseq.seq", beam;
option, warn;


/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="./jmad/tt2_fta.inp";

betx0 = initbeta0->betx;
bety0 = initbeta0->bety;
alfx0 = initbeta0->alfx;
alfy0 = initbeta0->alfy;
dx0   = initbeta0->dx;
dy0   = initbeta0->dy;
dpx0  = initbeta0->dpx;
dpy0  = initbeta0->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;


/***********************************
* Cleaning up
***********************************/
system, "rm tt2_repo";

stop;
