/*****************************************************
 * MADX optics file for MTE island extraction model 
 *
 * F.Velotti, M. Fraser, A. Haushauer
 *****************************************************/
 title, "PS/TT2/TT10 SFTPRO optics. Protons - 14 GeV/c";

 option, echo;
 option, RBARC=FALSE;

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/
system,"[ -d /afs/cern.ch/eng/acc-models/ps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/ps/2021 ps_repo";
system,"[ ! -e ps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-ps -b 2021 ps_repo";

system, "ln -fns ps_repo/scenarios/sftpro/6_extraction ps_sft_repo";


system, "ln -fns ./../../tt2 tt2_repo";
system, "ln -fns ./../../tt10 tt10_repo";

system, "ln -fns ./../line tt10_line_repo";

system, "ln -fns ./../../ps_ext_elements ps_extr_repo";
/*******************************************************************************
 * Beam
 *******************************************************************************/
Beam, particle=PROTON,pc=14;
BRHO      := BEAM->PC * 3.3356;

/*****************************************************************************
 * TT2
 *****************************************************************************/
 option, -echo;
 call, file = "./tt2_repo/tt2.ele";
 call, file = "tt10_line_repo/tt2_mte_2010.str";
 call, file = "./tt2_repo/tt2.seq";
 call, file = "./tt2_repo/tt2.dbx";

 option, echo;


/*******************************************************************************
 * TT10
 *******************************************************************************/

 call, file = "./tt10_repo/tt10.ele";
 call, file = "tt10_line_repo/tt10_mte_2010.str";
 call, file = "./tt10_repo/tt10.seq";
 call, file = "./tt10_repo/tt10.dbx";

/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/

 tt2tt10: sequence, refer=ENTRY, l = 1164.8409;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  tt10                  , at = 304.6954;
 endsequence;


/*****************************************************************************
 * Load initial twiss parameters which are then used for FTN optics
 *****************************************************************************/
! Those should be the correct initial conditions as obtained from the PS
call, file = "./tt2_mte_from_stitched_kickers.inp";

! Hystorical conditions and kept only until re-matching done
!call, file = "./tt2_mte_2010.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/


! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

exec, set_ini_conditions();


set,  format="10.5f";

use, sequence=tt2tt10;


/*******************************************************************************
 * Optics using initial conditions from stitched model
 *******************************************************************************/

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0;


/*****************************************************************************
 Calculate live initial condition for KR or any other changes in the ring
  - For MTE, the idea is to use the islands trajectory as nominal and 
  then see the core trajectory with respect to that
*****************************************************************************/

call, file = "./load_sftpro_extraction.madx";

set, format="22.6e";
exec, calculate_extraction(twiss_ps_tt2tt10_sftpro_stitched.tfs);

exec, set_ini_conditions();


use, sequence=tt2tt10;
select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_tt2tt10_sftpro.tfs";


! Make one single tfs file for both ring and FTN transfer line using kickers
len_twiss_tl = table(twiss, tablelength);

i = 2;
option, -info;
while(i < len_twiss_tl){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    !new_x = x;
    !new_px = px;
    fill, table=trajectory;

    i = i + 1;
};

option, info;
write, table=trajectory, file="twiss_ps_tt2tt10_sftpro_nom_complete.tfs";

/************************************
* Cleaning up
************************************/
system, "rm ps_repo";
system, "rm ps_sft_repo";
system, "rm ps_extr_repo";
system, "rm tt2_repo";
system, "rm tt10_line_repo";
system, "rm tt10_repo";
stop;
