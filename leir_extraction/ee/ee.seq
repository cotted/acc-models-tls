!------------------------------------------------------------
! EE sequence file, O.Berrig 2008
!
! The EE line goes from the extraction septum in LEIR to the ETL towards the PS
! Based on EE extraction line in MAD8 from C. Carli
! Converted to MADX   Jan 2008 O.Berrig
!------------------------------------------------------------
/**************************************************
 * Bending magnets
 **************************************************/
  dEEB1 = 0.267710369431;
  Ifr   = 0.35;


/**************************************************
 * Quadrupole magnets
 **************************************************/
! Optics from 2016 (Alex and Verena)
/* kee.q1 = 0.900802; */
/* kee.q2 = -0.7024; */
/* kee.q3 = 0.471543; */

! Rematching from Reyes and Enrico to match to PS
kee.q1 := 9.34865e-01; 
kee.q2 := -7.31526e-01;
kee.q3 := 5.33233e-01;
/**************************************************
 * Bending magnets
 **************************************************/
  lEEB1 = 1.175;     !  magnetic length of Bendings EEB
  EE.B1       : SBEND, L= lEEB1*dEEB1/(2*sin(dEEB1/2.))
                     , ANGLE= dEEB1, E1= dEEB1/2., E2= dEEB1/2.
                     , HGAP= 0.07, FINT=Ifr;

  EE.B12      : MARKER;


/**************************************************
 * Quadrupole magnets
 **************************************************/
  EE.Q1       : QUADRUPOLE, L=0.499, K1:=KEE.Q1;
  EE.Q2       : QUADRUPOLE, L=0.499, K1:=KEE.Q2;
  EE.Q3       : QUADRUPOLE, L=0.499, K1:=KEE.Q3;


/**************************************************
 * Corrector magnets
 **************************************************/
  EE.DHV1     :    KICKER;


/**************************************************
 * Monitors
 **************************************************/
  EE.MTV1     :    MARKER;
  EE.UHV1     :    MONITOR;
  EE.UHV2     :    MONITOR;


 /**************************************************
 * Vacuum chambers
 **************************************************/
  EE.VC010    :    MARKER;
  EE.VC020    :    MARKER;
  EE.VC030    :    MARKER;
  EE.VC040    :    MARKER;
  EE.VC050    :    MARKER;
  EE.VC060    :    MARKER;
  EE.VC070    :    MARKER;
  EE.VC080    :    MARKER;
  EE.VC090    :    MARKER;
  EE.VC100    :    MARKER;


EE: sequence, refer=centre, l = 11.02480;

! start close to LEIR
 
  EE.START    :  marker     , at =    0.00000;
  EE.DHV1                   , at =    0.00000;
  EE.VC010                  , at =    0.50025;
  EE.Q1                     , at =    1.25000;
  EE.VC020                  , at =    2.10000;
  EE.Q2                     , at =    2.95000;
  EE.VC030                  , at =    3.31655;
  EE.MTV1                   , at =    3.43360;
  EE.VC040                  , at =    4.00235;
  EE.UHV1                   , at =    4.57110;
  EE.VC050                  , at =    4.87855;
  EE.B1                     , at =    5.77530;
  EE.VC060                  , at =    6.49310;
  EE.B12                    , at =    6.62170;
  EE.VC070                  , at =    6.75030;
  EE.B1                     , at =    7.46820;
  EE.VC080                  , at =    8.22060;
  EE.Q3                     , at =    8.63320;
  EE.VC090                  , at =    9.12470;
  EE.UHV2                   , at =    9.36660;
  EE.VC100                  , at =   10.19570;
  EE.STOP     :  marker     , at =   11.02480;

endsequence;
