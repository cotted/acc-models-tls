!*******************************************
! TT41 model to AWAKE plasma cell
!
! C. Bracco, F.M. Velotti
!
! 20/07/2020: FV: - Fix of apertures (they were all
!                 declared as full aperture!)
!                 - Clened all variables used in sequnce
!                 generation. Used save sequence.
!*******************************************

TITLE, s='TT41 moddel';

option, echo;
option, RBARC=FALSE;

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load model
***************************************/

option, -warn;
    call, file = "./tt40tt41_awake.seq";
option, warn;

! There are other srtengths files, but these are only for special runs or MD
! the changes are done using a knob directly in trim editor 
call, file = "./str/tt40tt41_strength.str";

/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/

call, file = "./../stitched/tt40tt41_nominal.inp";

set, format="15.9f";
/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();

/***************************************
* Beam definition
***************************************/

Beam, particle = proton, pc = 400.0,exn=3.5e-6, eyn=3.5e-6,
sige=0.4e-3, NPART=2E11, BUNCHED;


use, sequence= TT40TT41, range=pt.extraction/#e;


select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, mux, muy, aper_1, aper_2;
twiss, beta0=initbeta0, file = "./twiss_tt40tt41_nom.tfs";

