!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! 11 November 2016 - M.A. Fraser, F. Velotti
!!
!! In this update, we fix a problem in the survey generation caused by the 
!! changes to the extraction settings from Q26 to Q20, which in fact
!! modified the MAD-X geometry of the line. The beam's initial coordinates
!! are now taken as input and matched back onto the correct transfer line axis
!! using correctors. The transfer line geometry is then as installed and
!! recorded in GEODE. The beam coordinates extraction settings (kickers and
!! bumpers) are taken from those stated in CERN-ATS-Note-2012-095 TECH
!! (E. Gianfelice-Wendt) except for the MSE current that are taken from
!! operational experience: kmse618 = 1.745e-3; mke6 voltage = 33.10 kV; 
!!
!! 
!! The effect of this update on the optics is negligible (seen on the DX, DY)
!! and amounts to a few small angle RBENDs implemented as errors (correctors).

!! F.Velotti - 11/2020: removal of extraction point correction from Q26 -> Q20. Difference in optics tiny and difficult to handle in 
!!                      stitched model
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

option, RBARC=FALSE;
option, echo;

title,   "TI2 model post-LS2 for SPS Q20 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt60ti2 ti2_repo";

set,    format="18.12f";
option, -echo, warn, info;

/***************************************
* Load model
***************************************/

call,    file = "ti2_repo/ti2.seq";
call,    file = "ti2_repo/ti2_apertures.dbx";
call,    file = "./ti2_liu.str";

option,  echo, warn, info;

beam,    sequence=ti2, particle=proton, pc= 450;
use,     sequence=ti2;

! Errors on b2 and b3 as measured
call, file="ti2_repo/mbi_b3_error.madx";

! Using initial dipoles to correct extraction trajectory caused by changing optics in the SPS Q26 => Q20
!call, file="./trajectory_error_q26_to_q20.madx";


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Q20 postion - Q26 position, relative to nominal axis at SPS extraction
!! "TI2$START"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!x0 = 0.281374274222 - 0.277702013;  !Q20 (MSE as in machine) - Q26 position at extraction (SPS)
!xp0 = 0.008696204806 - 0.008874135741;  !Q20 (MSE as in machine) - Q26 angle at extraction (SPS)

call, file = "./../stitched/sps_tt60_ti2_lhc_q20.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0;

};

exec, set_ini_conditions();

show, INITBETA0;

select, flag=twiss, clear;
select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, mux, muy;
set,    format="27.17f";
twiss, file="./twiss_ti2_nom.tfs", beta0=INITBETA0,x=x0,px=xp0; 

select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey, file="./survey_ti2.tfs",
          x0=x0.ti2, y0=y0.ti2, z0=z0.ti2,
          theta0=theta0.ti2, phi0=phi0.ti2, psi0=psi0.ti2;

/***********************************
* Cleaning up
***********************************/
system, "rm ti2_repo";
