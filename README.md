# TLs repo

## Guidelines
 - No capital letters in file and folder names
 - Divide between __extraction__ (upstream machine + TL ending to downstream machine) and __injection__ (TL starting at extraction of upstream machine + downstream machine)
 - One folder for each line and optics
 - One folder for pure TL and one with stitched model => only for extraction! Injection has only stitched with the downstream machine
    - In case share same elements, proposed to have one folder for the general layout
 - For each line we should have:
    - .dbx file for apertures 
    - .ele file for elements definition
    - .seq for sequence definition
    - .str file for strength (can be more if more lines in one, e.g. TT2 + FTA)
    - general_tlname.madx file for general input script => this name will also be the name of the different plots for the website
    - .tfs all output files
    - Inside the main folder of a TL group (ex. PS extraction), one can put a 'description.txt' file as done for the other line to add a brief description which will go in the website.

## Update output files
 - To update quickly all output files for a given part of the repo, or even the full repo, the command to run is:
 ```shell
python _scripts/utils_tls/run_madx_files.py <path/to/madx> <folder_name>
```
 - `<folder_name>` can be any of the top folders, like: `ps_extraction`. This can also be `all` to run all `general*.madx` files in the repo.
 - `<path/to/madx>` is the local madx installation

## Deployment
 - Before deploying, one should run the test:
```shell
cd _scripts
./test_repo.sh
```
 - if this is successful, then you can ask issue a merge request.

## This is for the old repo:
 - 2 python scripts have to be executed to prepare the website pages from the optics files
    - they do not automatically run MADX at the moment, this can be added
    - "mkdocks serve" command should be executed in the initial directory to start a local server to render the website created

## Checks
 - Checks that we should be doing:
    - Optics 
    - Survey 
    - Optics vs operational one